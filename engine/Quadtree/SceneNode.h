#pragma once

#include "../scene/Actor.h"
#include "../structures/AABB.h"
#include "../structures/DynamicArray.h"

class SceneNode
{
private:

	Actor* actor;			// Pointer to actor
	AABB rectBound;			// Actor's colision boundary

public:
	SceneNode();
	~SceneNode();

	void operator=( SceneNode& obj )
	{
		rectBound = obj.rectBound;
		actor = obj.actor;
	}
};

