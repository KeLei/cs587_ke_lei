#pragma once

#include "SceneNode.h"

class QuadNode
{
private:

	DynamicArray< SceneNode* > sceneNodes;		// Scene node contained in this tree node
	AABB nodeBound;								// bound of this tree node

	QuadNode* tlChild;							// Top-left child of this node
	QuadNode* trChild;							// Top-right child of this node
	QuadNode* dlChild;							// Down-left child of this node
	QuadNode* drChild;							// Down-right child of this node



public:

	QuadNode();
	~QuadNode();

	// insert to child node
	void InsertToTopLeft( QuadNode* node );
	void InsertToTopRight( QuadNode* node );
	void InsertToDownLeft( QuadNode* node );
	void InsertToDownRight( QuadNode* node );

	// Set boundary
	void SetBound( AABB boundary );

};