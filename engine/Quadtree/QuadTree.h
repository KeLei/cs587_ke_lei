#pragma once

#include "QuadNode.h"

class QuadTree
{

private:

	QuadNode* root;				// The root of the tree


public:

	QuadTree();
	~QuadTree();

	// Insert an actor to the tree
	void Insert( Actor* actor );

	// Traverse throught the tree
	void Traverse();
};