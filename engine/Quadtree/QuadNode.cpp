#include "QuadNode.h"

QuadNode::QuadNode() :
	tlChild( NULL ),
	trChild( NULL ),
	dlChild( NULL ),
	drChild( NULL )
{
}

QuadNode::~QuadNode()
{
	if( tlChild != NULL )
	{
		delete tlChild;
	}
	if( trChild != NULL )
	{
		delete trChild;
	}
	if( dlChild != NULL )
	{
		delete dlChild;
	}
	if( drChild != NULL )
	{
		delete drChild;
	}
}

void QuadNode::InsertToTopLeft( QuadNode* node )
{
	tlChild = node;
}

void QuadNode::InsertToTopRight( QuadNode* node )
{
	trChild = node;
}

void QuadNode::InsertToDownLeft( QuadNode* node )
{
	dlChild = node;
}

void QuadNode::InsertToDownRight( QuadNode* node )
{
	drChild = node;
}

void QuadNode::SetBound( AABB boundary )
{
	nodeBound = boundary;
}