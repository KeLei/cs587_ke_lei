#pragma once

#include <SDL.h>
#include "../interface/ITickable.h"
#include "../interface/IEventSubScriber.h"
#include "../interface/ICollisionHandler.h"
#include "Actor.h"

class ActorController : public ITickable, public IEventSubScriber, public ICollisionHandler
{

protected:

	Actor* actor;					//actor to be controled

public:

	ActorController();											// Default contructor
	ActorController( Actor* actor );							// Contructor with defined paraments
	~ActorController();											// Destructor

	void operator=( ActorController & actorController );		// Assignment operator overload

	void SetActor( Actor* actor );							// Setup controller

	virtual void MoveTo( Vector2D position );					// Move the actor right

	virtual void Tick();										// Tickable interface and controls the actor
	virtual void HandleUserEvent( SDL_Event* incomingEvent );	// Update the actor to deal with event
	virtual void Update( Event incomingEvent );					// Update the actor according to the event
	virtual void OnCollision( Physics* physicsBeforeCollision, Actor* collisionTaker);			// Handle collision
};
