#include "Physics.h"
#include <math.h>
#include <iostream>

Physics::Physics()
{
	boundaryX = 0.0f;
	boundaryY = 0.0f;

	maxSpeed = 0.0f;

	collidable = false;

	blockXPositive = false;
	blockXNegative = false;
	blockYPositive = false;
	blockYNegative = false;
}

Physics::Physics( SDL_Rect* rectangle )
{
	position.SetVector2D( ( float )rectangle -> x, ( float )rectangle -> y);

	velocity.SetVector2D( 0.0f, 0.0f );

	boundaryX = ( float )rectangle -> w + rectangle -> x;
	boundaryY = ( float )rectangle -> h + rectangle -> y;

	collidable = true;

	blockXPositive = false;
	blockXNegative = false;
	blockYPositive = false;
	blockYNegative = false;
}

Physics::Physics( Physics* physicsObj )
{
	position = physicsObj -> position;			
	velocity = physicsObj -> velocity;
	gravity = physicsObj -> gravity;			

	acceleration = physicsObj -> acceleration;

	boundaryX = physicsObj -> boundaryX;
	boundaryY = physicsObj -> boundaryY;

	maxSpeed = physicsObj -> maxSpeed;

	collidable = physicsObj -> collidable;
	orientation = physicsObj -> orientation;

	blockXPositive = physicsObj -> blockXPositive;
	blockXNegative = physicsObj -> blockXNegative;
	blockYPositive = physicsObj -> blockYPositive;
	blockYNegative = physicsObj -> blockYNegative;
}

Physics::~Physics()
{}

void Physics::operator=( Physics& physicsObj )
{
	position = physicsObj.position;			
	velocity = physicsObj.velocity;
	gravity = physicsObj.gravity;			

	acceleration = physicsObj.acceleration;

	boundaryX = physicsObj.boundaryX;
	boundaryY = physicsObj.boundaryY;

	maxSpeed = physicsObj.maxSpeed;

	collidable = physicsObj.collidable;
	orientation = physicsObj.orientation;
}

void Physics::SetAABB( float posX, float posY, float width, float height )
{
	position.SetVector2D( posX, posY );

	boundaryX = posX + width;
	boundaryY = posY + height;
}

void Physics::SetVelocity( Vector2D velocity )
{
	this -> velocity = velocity;
}

void Physics::SetPosition( Vector2D position )
{
	boundaryX += position.x - this -> position.x;
	boundaryY += position.y - this -> position.y;
	this -> position = position;
}

void Physics::SetGravity( Vector2D gravity )
{
	this -> gravity = gravity;
}

void Physics::SetXVelocity( float velocityX )
{
	velocity.x = velocityX;
}

void Physics::SetYVelocity( float velocityY )
{
	velocity.y = velocityY;
}

void Physics::SetXBoundary( float boundaryX )
{
	this -> boundaryX = boundaryX;
}

void Physics::SetYBoundary( float boundaryY )
{
	this -> boundaryY = boundaryY;
}

void Physics::SetXAcceleration( float acceleration )
{
	this -> acceleration.x = acceleration;
}

void Physics::SetYAcceleration( float acceleration )
{
	this -> acceleration.y = acceleration;
}

void Physics::SetXCoordinate( float coordinateX )
{ 
	position.x = coordinateX;
}

void Physics::SetYCoordinate( float coordinateY )
{ 
	position.y = coordinateY;
}

void Physics::SetMaxSpeed( float speed )
{ 
	maxSpeed = speed;
}

void Physics::SetCollidable()
{
	collidable = true;
}

void Physics::SetIncollidable()
{
	collidable = false;
}

void Physics::Move( Vector2D delta )
{
	position.x += delta.x;
	position.y -= delta.y;

	boundaryX += delta.x;
	boundaryY -= delta.y;
}

void Physics::Move()
{
	// Reduce y velocity with gravity
	velocity.y -= gravity.y / 100.0f;
	velocity.x += acceleration.x;

	if( abs( acceleration.x ) > 0.0f && abs( velocity.x ) >= maxSpeed )
	{
		// If speed exceed max speed
		velocity.x = velocity.x > 0.0f ? maxSpeed : -maxSpeed;
	}
	else if( abs( acceleration.x ) > 0.0f && abs( velocity.x ) < abs( acceleration.x ) )
	{
		// If speed becomes 0
		velocity.x = 0.0f;
		acceleration.x = 0.0f;
	}

	position.x += velocity.x;
	boundaryX += velocity.x;

	position.y -= velocity.y;
	boundaryY -= velocity.y;

	UnblockMovement();
}

Vector2D& Physics::GetVelocity()
{
	return velocity;
}

Vector2D& Physics::GetPosition()
{
	return position;
}

float Physics::GetXAcceleration()
{
	return acceleration.x + gravity.x;
}

float Physics::GetYAcceleration()
{
	return acceleration.y + gravity.y;
}

float Physics::GetXBoundary()
{
	return boundaryX;
}

float Physics::GetYBoundary()
{
	return boundaryY;
}

float Physics::GetMaxSpeed()
{
	return maxSpeed;
}

Vector2D Physics::GetGravity()
{
	return gravity;
}

bool Physics::IsCollidable()
{
	return collidable;
}

bool Physics::IsCollideWith( Physics* actorPhysics )
{

	return ( boundaryX > actorPhysics -> GetPosition().x &&
		boundaryY > actorPhysics -> GetPosition().y && 
		position.x < actorPhysics -> GetXBoundary() && 
		position.y < actorPhysics -> GetYBoundary() );
}

void Physics::BlockXPositive()
{
	velocity.x = 0;
	blockXPositive = true;
}

void Physics::BlockXNegative()
{
	velocity.x = 0;
	blockXNegative= true;
}

void Physics::BlockYPositive()
{
	velocity.y = 0;
	blockYPositive = true;
}

void Physics::BlockYNegative()
{
	velocity.y = 0;
	blockYNegative= true;
}

bool Physics::MoveableXPositive()
{
	return !blockXPositive;
}

bool Physics::MoveableXNegative()
{
	return !blockXNegative;
}

bool Physics::MoveableYPositive()
{
	return !blockYPositive;
}

bool Physics::MoveableYNegative()
{
	return !blockYNegative;
}

void Physics::UnblockMovement()
{
	blockXPositive = false;
	blockXNegative = false;
	blockYPositive = false;
	blockYNegative = false;
}
