#include "Actor.h"

Actor::Actor()
{
	type = "";
	id = -1;
	alive = false;
	selected = false;

	drawable = new Drawable;
	physics = new Physics;
	inventory = new Inventory;
}


Actor::Actor(	int id,
			 std::string type, 
			 bool alive, 
			 Drawable* drawable, 
			 Physics* physics,
			 Inventory* inventory,
			 bool selected )
{
	this -> type = type;
	this -> alive = alive;
	this -> drawable = drawable;
	this -> physics = physics;
	this -> inventory = inventory;
	this -> selected = selected;
}

Actor::Actor( Actor* actor )
{
	this -> id = actor -> id;
	this -> type = actor -> type;
	this -> alive = actor -> alive;
	this -> drawable = actor -> drawable;
	this -> physics = actor -> physics;
	this -> inventory = actor -> inventory;
	this -> selected = actor -> selected;
}

void Actor::operator=( Actor & actor )
{
	this -> id = actor.id;
	this -> type = actor.type;
	this -> alive = actor.alive;
	this -> drawable = actor.drawable;
	this -> physics = actor.physics;
	this -> inventory = actor.inventory;
	this -> selected = actor.selected;
}

void Actor::SetActor(	int id,
					 std::string type,
					 bool alive,
					 Drawable* drawable,
					 Physics* physics,
					 Inventory* inventory,
					 bool selected )
{
	this -> type = type;
	this -> alive = alive;
	this -> drawable = drawable;
	this -> physics = physics;
	this -> inventory = inventory;
	this -> selected = selected;
}

void Actor::Draw( SDL_Renderer* renderer, float CameraOffsetX, float CameraOffsetY, float cameraSpeedScale )
{
	drawable -> Draw( renderer, CameraOffsetX, CameraOffsetY, cameraSpeedScale );		
}

void Actor::SetTexture( SDL_Texture* texture, float scale )
{
	// Define the size of the texture
	int width = 0;
	int height = 0;

	// Get texture size
	SDL_QueryTexture( texture, NULL, NULL, &width, &height );

	// Setup texture and rectangle to actor
	drawable -> SetTexture( texture );
	drawable -> GetRect() -> w = ( int )( ( float )width * scale );
	drawable -> GetRect() -> h = ( int )( ( float )height * scale );

	// Set physic position
	physics -> SetAABB( 
		physics -> GetPosition().x, 
		physics -> GetPosition().y, 
		( float )width * scale , 
		( float )height * scale );
}

void Actor::SetTexture( SDL_Texture* texture, Vector2D size)
{
	// Setup texture and rectangle to actor
	drawable -> SetTexture( texture );
	drawable -> GetRect() -> w = ( int )size.x;
	drawable -> GetRect() -> h = ( int )size.y;

	// Set physic position
	physics -> SetAABB( 
		physics -> GetPosition().x, 
		physics -> GetPosition().y, 
		size.x, 
		size.y );
}

Drawable* Actor::GetDrawable()
{
	return drawable;
}

Physics* Actor::GetPhysics()
{
	return physics;
}

Inventory* Actor::GetInventory()
{
	return inventory;
}

bool Actor::IsSelected()
{
	return selected;
}

void Actor::SetSelection( bool selected )
{
	this -> selected = selected;
	return;
}

void Actor::SetType( std::string type )
{
	this -> type = type;
	return;
}

void Actor::SetID( int id )
{
	this -> id = id;
	return;
}

void Actor::SetPosition( Vector2D position )
{
	// Set drawable position
	drawable -> GetRect() -> x = ( int )position.x;
	drawable -> GetRect() -> y = ( int )position.y;

	// Set physic position
	physics -> SetAABB( position.x, position.y, ( float )drawable -> GetRect() -> w , ( float )drawable -> GetRect() -> h );
}

std::string Actor::GetType()
{
	return type;
}

int Actor::GetID()
{
	return id;
}
Actor::~Actor()
{
}