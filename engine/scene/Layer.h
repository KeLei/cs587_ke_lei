#pragma once

#include <SDL.h>
#include "../interface/ITickable.h"
#include "../resource/Text.h"
#include "../structures/DynamicArray.h"
#include "../event/EventBus.h"
#include "../interface/IEventSubscriber.h"
#include "../event/Event.h"
#include "../camera/Camera.h"
#include "../input/KeyboardHandler.h"
#include "Actor.h"

class Layer
{

private:

	DynamicArray< Actor* > actorList;						// List of actors to be rendered
	/*
	*TODO: Use quad tree
	*/
	DynamicArray< Actor* > collidableList;					// List of collidable actors
	DynamicArray< Text* > textList;							// List of texts

	float speedScale;

public:

	Layer();
	~Layer();

	// Render the scene
	void Render( SDL_Renderer* renderer, Camera camera );	

	// Add an actor to scene
	void AddToScene( Actor* actor );

	// Add a text
	void AddText( Text* text );								

	// Get the actor list in this layer
	DynamicArray< Actor* >* GetActorList();								

	// how this layer moves according to camera
	void SetSpeedToCamera( float speedScale );
};

