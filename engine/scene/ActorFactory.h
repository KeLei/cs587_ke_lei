#pragma once

#include "Actor.h"
#include "../json/JsonParser.h"

class ActorFactory
{
protected:

	static JsonValue* jsonObj;						// Store actor properties
	static HashMap< JsonValue* > actorMap;			// Stores the actor configurations by their name
	
public:
	
	static int ActorFactory::actorID;
	
	ActorFactory();
	~ActorFactory();

	// Get actor according to the name of the actor
	static Actor* GetActor( string actorName, SDL_Renderer* renderer );
};

