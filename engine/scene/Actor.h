#pragma once

#include "../interface/IPhysics.h"
#include "Drawable.h"
#include "Physics.h"
#include "Inventory.h"
#include "../json/JsonParser.h"
#include <string>

class Actor
{

protected:

	int id;										// Actor ID
	std::string type;							// Actor type
	bool alive;									// If the actor is alive
	bool selected;								// If the actor is selected

	Drawable* drawable;							// Drawable component
	Physics* physics;							// Physics property component
	Inventory* inventory;						// Inventory component

public:

	Actor();

	Actor(	int id,
		std::string type, 
		bool alive, 
		Drawable* drawable, 
		Physics* physics,
		Inventory* inventory,
		bool selected );

	Actor( Actor* actorObj );	

	~Actor();

	// Assignment operator overload
	void operator= ( Actor & actor );			

	// Draw the actor according to the camera position
	void Draw( SDL_Renderer* renderer, float CameraOffsetX, float CameraOffsetY, float cameraSpeedScale );		

	// Set the actor with defined paraments
	void SetActor(	int id,
					std::string type, 
					bool alive, 
					Drawable* drawable, 
					Physics* physics,
					Inventory* inventory,
					bool selected );			

	// Set the actor texture
	void SetTexture( SDL_Texture* texture, float scale );

	// Set the actor texture with size assigned
	void SetTexture( SDL_Texture* texture, Vector2D size );

	// Determine whether the actor is selected
	void SetSelection( bool selected );			

	// Set the actor type
	void SetType( std::string type );			

	// Set the actor id
	void SetID( int id );			

	// Set the actor position
	void SetPosition( Vector2D position );		

	// Get pointer to drawable object
	Drawable* GetDrawable();					

	// Get pointer to physics object
	Physics* GetPhysics();						

	// Get pointer to inventory object
	Inventory* GetInventory();					

	// Get the type of the actor
	std::string GetType();						

	// Get the id of the actor
	int GetID();								

	// Determine whether the actor is selected
	bool IsSelected();							
};