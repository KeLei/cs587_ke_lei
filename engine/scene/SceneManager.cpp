#include "SceneManager.h"

SceneManager* SceneManager::instance = NULL;

SceneManager* SceneManager::GetInstance()
{
	if( instance == NULL )
	{
		instance = new SceneManager;
	}
	return instance;
}

void SceneManager::Render()
{
	SDL_RenderClear( renderer );

	camera.Update();

	for( int i = 0; i < layerList.Size(); i++ )
	{
		layerList[ i ] -> Render( renderer, camera );
	}

	// Render Text
	//for( int i = 0; i < textList.Size(); i++ )
	//{
	//	// Loop to render text
	//	textList[ i ] -> Draw();
	//}
	SDL_RenderPresent( renderer );
}

void SceneManager::RunTicks()
{
	for( int i = 0; i < ITickableList.Size(); i++ )
	{
		// Loop to execute every functions to be ticked
		ITickableList[ i ] -> Tick();
	}
}

void SceneManager::AddToScene( Actor* actor, std::string layerName )
{
	layerList[ layerMap[ layerName ] ] -> AddToScene( actor );
}

void SceneManager::AddText( Text* text, std::string layerName )
{
	layerList[ layerMap[ layerName ] ] -> AddText( text );
}

void SceneManager::AddToTickable( ITickable* tickable )
{
	ITickableList.PushBack( tickable );
	//layerList[ layerIndex ] -> AddToTickable( tickable );
}

void SceneManager::SetRenderer( SDL_Renderer* renderer )
{
	this -> renderer = renderer;
}

void SceneManager::SetCamera( float viewportX, 
							 float viewportY,
							 float viewportW,
							 float viewPortH,
							 float rangeX,
							 float rangeY,
							 float rangeW,
							 float rangeH)
{
	camera.SetViewport( viewportX, viewportY, viewportX + viewportW, viewportY + viewPortH );
	camera.SetActionRange( rangeX, rangeY, rangeX + rangeW, rangeY + rangeH );
}

void SceneManager::SetCameraWatcher( Actor* actor )
{
	camera.Follow( actor );
}

void SceneManager::AddLayer( std::string layerName )
{
	Layer* newLayer = new Layer;
	layerList.PushBack( newLayer );
	layerMap[ layerName ] = layerList.Size() - 1;
}

void SceneManager::SetPrimaryLayer( std::string layerName )
{
	layerList[ layerMap[ layerName ] ] ->SetSpeedToCamera( 1.0f );
}

//void SceneManager::RemoveFromScene( int id )
//{
//	this -> renderer = renderer;
//	for( int i = 0; i < actorList.Size(); i++ )
//	{
//		if( actorList[ i ] -> GetID() == id )
//		{
//			actorList.Erase( i );
//			break;
//		}
//	}
//}
//
//void SceneManager::MoveToFirst( int index )
//{
//	if ( actorList.Size() > 1 )
//	{
//		Actor* selectedActor = actorList[ index ];
//		actorList.Erase( index );
//		actorList.PushFront( selectedActor );
//	}
//}

DynamicArray< Actor* >* SceneManager::GetCollidableList( int actorID, std::string layerName )
{
	/*
	*TODO: Add quad tree query
	*/
	return layerList[ layerMap[ layerName ] ] -> GetActorList();
}

Layer* SceneManager::GetLayer( std::string layerName )
{
	return layerList[ layerMap[ layerName ] ];
}