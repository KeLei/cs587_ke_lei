#include "ActorController.h"
#include "math.h"

ActorController::ActorController()
{
	actor = NULL;
}

ActorController::ActorController( Actor* actor )
{
	this -> actor = actor;
}

ActorController::~ActorController()
{}

void ActorController::SetActor( Actor* actor )
{
	this -> actor = actor;
}

void ActorController::Tick()
{
}

void ActorController::operator= ( ActorController & actorController )
{
	this -> actor = actorController.actor;
}

void ActorController::MoveTo( Vector2D position )
{
	actor -> GetPhysics() -> SetPosition( position );
}

void ActorController::Update( Event incomingEvent )
{
	// handle event
}

void ActorController::HandleUserEvent( SDL_Event* incomingEvent )
{
	// Handle User Input
}

void ActorController::OnCollision( Physics* physicsBeforeCollision,  Actor* collisionTaker )
{
	// Handle collision
}

