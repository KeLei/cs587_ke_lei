#include "Layer.h"


Layer::Layer()
{
}

Layer::~Layer()
{
}

void Layer::Render( SDL_Renderer* renderer, Camera camera )
{
	for( int i = 0; i < actorList.Size(); i++ )
	{
		// Loop to render every objects in the scene
		actorList[ i ] -> Draw( 
			renderer, 
			camera.GetViewport().topLeftX, 
			camera.GetViewport().topLeftY,
			speedScale );
	}

	// Render Text
	for( int i = 0; i < textList.Size(); i++ )
	{
		// Loop to render text
		textList[ i ] -> Draw();
	}
}


void Layer::AddToScene( Actor* actor )
{
	actorList.PushBack( actor );
	if( actor -> GetPhysics() -> IsCollidable() )
	{
		collidableList.PushBack( actor );
	}
}

void Layer::AddText( Text* text)
{
	textList.PushBack( text );
}

DynamicArray< Actor* >* Layer::GetActorList()
{
	return &actorList;
}

void Layer::SetSpeedToCamera( float speedScale )
{
	this -> speedScale = speedScale;
}
