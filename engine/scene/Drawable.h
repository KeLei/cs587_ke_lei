#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include "../structures/DynamicArray.h"
#include "../json/JsonParser.h"
#include "../timer/Timer.h"

class Drawable
{
private:

	bool drawable;									// If the object is drawable

	SDL_Texture* texture;							// Pointer to the texture of the object
    SDL_Renderer* renderer;							// Pointer to the renderer of the object
	SDL_Rect rectTexture;							// Rectangle of texture

	DynamicArray< JsonValue* >* rectClips;			// Stores the clipping rectangles of the drawable
	JsonValue* rectangles;							// Json rectangles for animation

	int currentRect;								// Index of current rectangle that clips the texture
	int speed;										// The time interval of switcing rectangles

	Timer timer;									// Timer used for animation

public:

	Drawable();										// Default contructor
	Drawable( SDL_Texture* texture, 
				SDL_Renderer* renderer );			// Contruct the object with defined texture, renderer and texture rectangle

	~Drawable();					


	void Draw( SDL_Renderer* renderer,
		float CameraOffsetX,
		float CameraOffsetY,
		float cameraSpeedScale );
													
	// Draw the object with clip and camera position
	bool IsDrawable();								
	// Return whether the object is drawable

	// Setup obejct renderer
	void SetRenderer( SDL_Renderer* renderer );		

	// Setup object texture rectangle
	void SetRect( SDL_Rect rectTexture );			

	// Setup object texture
	void SetTexture( SDL_Texture* texture );		

	// Set speed of the animation
	void SetSpeed( int speed );					

	// Set the rectangle clips of the drawable object
	void SetClipRectangle( const char* clipName );	

	// Load clip rectangles throught a json file
	void LoadRects( const char* filePath );

	// Get pointer to the texture rectangle
	SDL_Rect* GetRect();							

	// Get pointer to the renderer
	SDL_Renderer* GetRenderer();					

	
};

