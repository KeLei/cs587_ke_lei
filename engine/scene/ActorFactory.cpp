#include "ActorFactory.h"
#include "../resource/ResourceManager.h"

int ActorFactory::actorID = 0;
JsonValue* ActorFactory::jsonObj = NULL;

ActorFactory::ActorFactory()
{
}

ActorFactory::~ActorFactory()
{
}

Actor* ActorFactory::GetActor( string actorName, SDL_Renderer* renderer )
{
	jsonObj = ResourceManager::GetInstance() -> GetActorConfig( actorName );

	Actor* newActor = new Actor;

	newActor -> GetDrawable() -> SetRenderer( renderer );

	// Set position
	newActor  -> SetPosition( Vector2D( ( ( *jsonObj )[ "position" ].GetValueArray() )[ 0 ] -> GetNumberValue(),
		( ( *jsonObj )[ "position" ].GetValueArray() )[ 1 ] -> GetNumberValue() ) );


	// Set texture
	if( ( ( *jsonObj )[ "size" ].GetValueArray() ).Size() == 1 )
	{
		// Scaled size
		newActor -> SetTexture( 
			ResourceManager::GetInstance() -> GetTexture( (( *jsonObj )[ "texture" ].GetStringValue()).c_str() ),  
			( ( *jsonObj )[ "size" ].GetValueArray() )[ 0 ] -> GetNumberValue() );
	}
	else
	{
		// Specified size
		newActor -> SetTexture( 
			ResourceManager::GetInstance() -> GetTexture( (( *jsonObj )[ "texture" ].GetStringValue()).c_str() ),  
			Vector2D( ( ( *jsonObj )[ "size" ].GetValueArray() )[ 0 ] -> GetNumberValue(),
			( ( *jsonObj )[ "size" ].GetValueArray() )[ 1 ] -> GetNumberValue() ) );
	}

	// Set rectangle clips
	if( ( *jsonObj )[ "initialClips" ].GetType() == STRING )
	{
		// If there's texture clips
		newActor -> GetDrawable() -> LoadRects( ( *jsonObj )[ "animationClips" ].GetStringValue().c_str() );
		newActor -> GetDrawable() -> SetSpeed( ( int )( *jsonObj )[ "animationSpeed" ].GetNumberValue() );
		newActor -> GetDrawable() -> SetClipRectangle( ( *jsonObj )[ "initialClips" ].GetStringValue().c_str() );
	}

	newActor -> GetPhysics() -> SetAABB( ( float )newActor -> GetDrawable() -> GetRect() -> x,
		( float )newActor -> GetDrawable() -> GetRect() -> y,
		( float )newActor -> GetDrawable() -> GetRect() -> w,
		( float )newActor -> GetDrawable() -> GetRect() -> h );

	if( ( *jsonObj )["collidable"].GetBooleanValue() )
	{
		newActor -> GetPhysics() -> SetCollidable();
	}
	else
	{
		newActor -> GetPhysics() -> SetIncollidable();
	}

	// Set type and ID
	newActor -> SetType( ( *jsonObj )[ "type" ].GetStringValue() );
	newActor -> SetID( actorID++ );

	return newActor;
}