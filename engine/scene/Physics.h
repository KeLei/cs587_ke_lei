#pragma once

#include <SDL.h>
#include "../structures/Vector2D.h"
#include "../interface/IPhysics.h"
#include "math.h"

class Physics
{
private:
	
	Vector2D position;			// Current position
	Vector2D velocity;			// Current velocity
	Vector2D gravity;			// Current gravity, indicates a overall moving trend of this object

	Vector2D acceleration;		// Current acceleration, positive or negative indicates direction

	float boundaryX;			// x bound of the object
	float boundaryY;			// y bound of the object

	float maxSpeed;				// maximum speed of the actor

	bool collidable;			// Whether the object is colliable
	bool orientation;			// Positive - facing right, negative - face left

	bool blockXPositive;		// Whether the movement to positive x is blocked
	bool blockXNegative;		// Whether the movement to negative x is blocked
	bool blockYPositive;		// Whether the movement to positive y is blocked
	bool blockYNegative;		// Whether the movement to negative y is blocked

public:

	// Default constructor
	Physics();			

	// Constructor with known rectangle
	Physics( SDL_Rect* rectangle);

	// Copy Constructor
	Physics( Physics* physicsObj );

	// Assignment operator
	void operator= ( Physics& physicsObj );

	// Destructor
	~Physics();

	// Set object speed
	void SetVelocity( Vector2D velocity );

	// Set object coordinate
	void SetPosition( Vector2D coordinate );

	// Set object gravity
	void SetGravity( Vector2D gravity );

	// Set object speed
	void SetXVelocity( float velocityX );

	// Set object speed
	void SetYVelocity( float velocityY );

	// Set object acceleration speed
	void SetXAcceleration( float acceleration );

	// Set object acceleration speed
	void SetYAcceleration( float acceleration );

	// Set object x coordinate
	void SetXCoordinate( float coordinateX );

	// Set object y coordinate
	void SetYCoordinate( float coordinateY );

	// Set object x coordinate
	void SetXBoundary( float boundaryX );

	// Set object y coordinate
	void SetYBoundary( float boundaryY );

	// Set object max speed
	void SetMaxSpeed( float maxSpeed);

	// Set object AABB
	void SetAABB( float topLeftX, float topLeftY, float downRightX, float downLeftY );

	// Set up whether the object is collidable
	void SetCollidable();
	void SetIncollidable();

	virtual void Move();
	void Move( Vector2D delta );

	// Get object speed
	Vector2D& GetVelocity();

	// Get object speed
	Vector2D& GetPosition();

	// Get gravity
	Vector2D GetGravity();

	// Get current x-coordinate acceleration
	float GetXAcceleration();

	// Get current y-coordinate acceleration
	float GetYAcceleration();

	// Get object x bound
	float GetXBoundary();

	// Get object y bound
	float GetYBoundary();

	// Get object y bound
	float GetMaxSpeed();

	// If the object is collidable
	bool IsCollidable();			

	// If the object collide with other object
	bool IsCollideWith( Physics* actorPhysics );

	// Block movement
	void BlockXPositive();
	void BlockXNegative();
	void BlockYPositive();
	void BlockYNegative();

	// Return whether the player is moveable in the direction
	bool MoveableXPositive();
	bool MoveableXNegative();
	bool MoveableYPositive();
	bool MoveableYNegative();

	// Unblock all movement
	void UnblockMovement();
};
