#include "Drawable.h"

Drawable::Drawable()
{
	drawable = false;
	texture = NULL;
	renderer = NULL;
	rectClips = NULL;

	currentRect = 0;
	rectTexture.x = 0;
	rectTexture.y = 0;
	rectTexture.w = 0;
	rectTexture.h = 0;
}

Drawable::Drawable( SDL_Texture *texture, SDL_Renderer* renderer )
{
	this -> drawable = true;
	this -> texture = texture;
	this -> renderer = renderer;
}

void Drawable::Draw( SDL_Renderer* renderer, float CameraOffsetX, float CameraOffsetY, float cameraSpeedScale )
{
	if ( drawable )
	{
		// Collision rectangle test
		//SDL_SetRenderDrawColor( renderer, 0xff, 0xff, 0xff, 0xff );		 
		//SDL_RenderDrawRect( renderer, &rectTexture );

		SDL_Rect drawRect = rectTexture;
		drawRect.x -= ( int )( CameraOffsetX * cameraSpeedScale );
		drawRect.y -= ( int )( CameraOffsetY * cameraSpeedScale );

		if( rectClips == NULL )
		{
			SDL_RenderCopy( renderer, texture, NULL, &drawRect );
		}
		else
		{
			if( timer.IsTimeout() )
			{
				// Change texture
				currentRect++;
				timer.RestartTimer();
			}
			if( currentRect >= ( *rectClips ).Size() )
			{
				// Loop the animation, ignore the speed value
				currentRect = 0;
			}

			SDL_Rect clipRect;
			clipRect.x = ( int )( ( *rectClips )[ currentRect ] -> GetValueArray() )[ 0 ] -> GetNumberValue();
			clipRect.y = ( int )( ( *rectClips )[ currentRect ] -> GetValueArray() )[ 1 ] -> GetNumberValue();
			clipRect.w = ( int )( ( *rectClips )[ currentRect ] -> GetValueArray() )[ 2 ] -> GetNumberValue();
			clipRect.h = ( int )( ( *rectClips )[ currentRect ] -> GetValueArray() )[ 3 ] -> GetNumberValue();
			
			drawRect.w = clipRect.w;
			drawRect.h = clipRect.h;
			rectTexture.w = drawRect.w;
			rectTexture.h = drawRect.h;

			SDL_RenderCopy( renderer, texture, &clipRect, &drawRect );
		}
	}
}

bool Drawable::IsDrawable()
{
	return drawable;
}

void Drawable::SetRenderer( SDL_Renderer* renderer )
{
	drawable = true;
	this -> renderer = renderer;
}

void Drawable::SetRect( SDL_Rect rectTexture )
{
	this -> rectTexture = rectTexture;
}

void Drawable::SetTexture( SDL_Texture* texture )
{
	this -> texture = texture;
}

void Drawable::SetSpeed( int speed)
{
	this -> speed = speed;
	timer.SetInterval( speed );
	timer.StartTimer();

}

void Drawable::SetClipRectangle( const char* clipName )
{
	rectClips = &( *rectangles )[ clipName ].GetValueArray();

	// Set current rectangle pointing to the first clip of the array
	currentRect = 0;
	rectTexture.w = ( int )( ( *rectClips )[ 0 ] -> GetValueArray() )[ 2 ] -> GetNumberValue();
	rectTexture.h = ( int )( ( *rectClips )[ 0 ] -> GetValueArray() )[ 3 ] -> GetNumberValue();

}

void Drawable::LoadRects( const char* filePath )
{
	JsonParser parser( filePath );
	rectangles = parser.Parse();
}

SDL_Rect* Drawable::GetRect()
{
	return &rectTexture;
}

SDL_Renderer* Drawable::GetRenderer()
{
	return renderer;
}

Drawable::~Drawable()
{}

