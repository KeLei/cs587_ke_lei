#pragma once

#include <SDL.h>
#include "../interface/ITickable.h"
#include "../resource/Text.h"
#include "../structures/DynamicArray.h"
#include "../structures/HashMap.h"
#include "../event/EventBus.h"
#include "../interface/IEventSubscriber.h"
#include "../event/Event.h"
#include "../camera/Camera.h"
#include "../input/KeyboardHandler.h"
#include "Actor.h"
#include "Layer.h"

class SceneManager
{

private:

	static SceneManager* instance;							// instance of the scene manager

	DynamicArray< Layer* > layerList;						// List of layers in the scene

	/*
	*TODO: Use quad tree
	*/
	DynamicArray< Actor* > collidableList;					// List of collidable actors
	DynamicArray< ITickable* > ITickableList;				// List of tickables to be ticked
	HashMap< int > layerMap;

	SDL_Renderer* renderer;									// Renderer of this scener

	Camera camera;

	SceneManager()											
	{
		renderer = NULL;
	}										


public:

	// Get the instance of the scene manager
	static SceneManager* GetInstance();						

	// Render the scene
	void Render();		

	// Run tickable objects in the scene
	void RunTicks();

	// Add an actor to specified layer
	void AddToScene( Actor* actor, std::string layerName );						

	// Add a text to specified layer
	void AddText( Text* text, std::string layerName );							

	// Add an tickable to the scene
	void AddToTickable( ITickable* tickable );			

	// Set the renderer of the scene
	void SetRenderer( SDL_Renderer* renderer );			

	// Setup camera
	void SetCamera( float viewportX, 
					float viewportY,
					float viewportW,
					float viewPortH,
					float rangeX,
					float rangeY,
					float rangeW,
					float rangeH);
													
	// Set the actor for the camera to follow
	void SetCameraWatcher( Actor* actor );					

	// Add a layer to the scene, return the index of the layer
	void AddLayer( std::string layerName );

	// Set primary layer that follows the camera without offset
	void SetPrimaryLayer( std::string layerName );

	// Remove actor from scene with specific id
	//void RemoveFromScene( int id );							

	// Move the specified actor to the first to be rendered
	//void MoveToFirst( int index );							

	// Get a list of collidable actors to the actor of given id
	DynamicArray< Actor* >* GetCollidableList( int actorID, std::string layerName );	

	// Get a layer
	Layer* GetLayer( std::string layerName );
};

