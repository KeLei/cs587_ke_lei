#pragma once

#include "../interface/ITickable.h"
#include "../interface/IState.h"
#include "../scene/Actor.h"

class StateMachine : public ITickable
{
private:

	IState* currentState;
	Actor* actor;

public:

	StateMachine();
	~StateMachine();

	void Tick();
	void Initialize( Actor* actor, IState* state );
};