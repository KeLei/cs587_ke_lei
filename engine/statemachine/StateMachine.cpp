#include "StateMachine.h"

StateMachine::StateMachine()
{
	currentState = NULL;
	actor = NULL;
}

StateMachine::~StateMachine()
{
}

void StateMachine::Initialize( Actor* actor, IState* state )
{
	this -> actor = actor;
	currentState = state;
}


void StateMachine::Tick()
{
	currentState -> Tick();
	if( ( currentState -> GetNextState() ).size() != 0 )
	{
	}
}