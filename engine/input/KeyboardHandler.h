#pragma once

#include <SDL.h>

enum KeyStatus
{
	KEY_PRESSED,
	KEY_JUST_PRESSED,
	KEY_RELEASED,
	KEY_JUST_RELEASED,
};

class KeyboardHandler
{

private:

	static KeyboardHandler* instance;
	KeyboardHandler();

public:

	// Recording key status
	KeyStatus keyStatus[ 323 ];
	KeyStatus lastKeyStatus[ 323 ];

	// Get the instance of the handler
	static KeyboardHandler* GetInstance();

	// Change current key status 
	void UpdateKeyStatus();

	// Update key pressing status
	void UpdateKeyStatus( SDL_Event event );

	~KeyboardHandler();
};

