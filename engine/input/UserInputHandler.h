#pragma once

#include "../scene/ActorController.h"
#include "../structures/DynamicArray.h"

class UserInputHandler
{

private:

	DynamicArray< ActorController* > actorControllerList;	// List of actor controller to be notified
	SDL_Event currentEvent;									// Current event


public:

	UserInputHandler();										// Default Constructor
	~UserInputHandler();									// Destructor

	bool UpdateInput();										// Handle user input
};