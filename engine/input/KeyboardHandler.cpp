#include "KeyboardHandler.h"
#include <iostream>

KeyboardHandler* KeyboardHandler::instance = nullptr;

KeyboardHandler::KeyboardHandler()
{
	for( int i = 0; i < 323; i++ )
	{
		keyStatus[ i ] = KEY_RELEASED;
		lastKeyStatus[ i ] = KEY_RELEASED;
	}
}

KeyboardHandler* KeyboardHandler::GetInstance()
{
	if( instance == nullptr )
	{
		instance = new KeyboardHandler;
	}
	return instance;
}

void KeyboardHandler::UpdateKeyStatus()
{
	for (int i = 0; i < 323; i++)
	{
		KeyStatus tempState = keyStatus[ i ];
		if( lastKeyStatus[ i ] == KEY_JUST_RELEASED )
		{
			keyStatus[ i ] = KEY_RELEASED;
		}
		if( lastKeyStatus[ i ] == KEY_JUST_PRESSED)
		{
			keyStatus[ i ] = KEY_PRESSED;
		}
		lastKeyStatus[ i ] = tempState;
	}
}


void KeyboardHandler::UpdateKeyStatus( SDL_Event event )
{
	if ( event.key.keysym.sym > 0 && 
		event.key.keysym.sym < 323 )
	{
		if( event.type == SDL_KEYDOWN && keyStatus[ event.key.keysym.sym ] != KEY_PRESSED )
		{
			keyStatus[ event.key.keysym.sym ] = KEY_JUST_PRESSED;
		}
		if( event.type == SDL_KEYUP && keyStatus[ event.key.keysym.sym ] != KEY_RELEASED )
		{
			keyStatus[ event.key.keysym.sym ] = KEY_JUST_RELEASED;
		}

	}
}

KeyboardHandler::~KeyboardHandler()
{
}
