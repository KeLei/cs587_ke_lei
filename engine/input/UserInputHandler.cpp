#include "UserInputHandler.h"
#include "KeyboardHandler.h"


UserInputHandler::UserInputHandler()
{}

UserInputHandler::~UserInputHandler()
{}

bool UserInputHandler::UpdateInput()
{
	while( SDL_PollEvent( &currentEvent ) )
	{
		if( currentEvent.type == SDL_QUIT || 
			( currentEvent.type == SDL_KEYUP && currentEvent.key.keysym.sym == SDLK_ESCAPE ) )
		{
			return false;
		}

		KeyboardHandler::GetInstance() -> UpdateKeyStatus( currentEvent );
	}
	KeyboardHandler::GetInstance() -> UpdateKeyStatus();
	return true;
}