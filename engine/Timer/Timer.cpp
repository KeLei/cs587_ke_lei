#include "Timer.h"


Timer::Timer()
{
	timeInterval = 0;
	timeElapsed = 0;
	started = false;
}


Timer::~Timer()
{
}

void Timer::SetInterval( int interval )
{
	timeInterval = interval;
}

void Timer::StartTimer()
{
	if( started == false )
	{
		started = true;
		startTime = std::clock();
	}
}

void Timer::RestartTimer()
{
	started = true;
	startTime = std::clock();
}

void Timer::StopTimer()
{
	started = false;
}

bool Timer::IsTimeout()
{
	if ( started )
	{
		timeElapsed = GetElapsedTime();
		if( timeElapsed >= timeInterval )
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	return false;
}

bool Timer::IsStarted()
{
	return started;
}

int Timer::GetElapsedTime()
{
	return std::clock() - startTime ;
}