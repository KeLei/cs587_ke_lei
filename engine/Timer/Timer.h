#pragma once

#include <time.h>
#include <ctime>

class Timer
{

private:

	std::clock_t startTime;
	int timeInterval;
	int timeElapsed;
	bool started;

public:

	Timer();
	~Timer();

	// Set time interval
	void SetInterval( int interval );

	// Set start time when this function is called
	void StartTimer();

	// Restart timer, use current time to calculate time interval
	void RestartTimer();

	// Stop timer and set everything to initial
	void StopTimer();

	// Update every frame to see if time is out
	bool IsTimeout();

	// Check if the timer is started
	bool IsStarted();

	// Get time interval between start time and the time this function is called
	int GetElapsedTime();
};

