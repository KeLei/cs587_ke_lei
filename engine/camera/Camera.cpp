#include "Camera.h"


Camera::Camera()
{
	actor = NULL;
}


Camera::~Camera()
{
}

void Camera::SetViewport( float topLeftX, float topLeftY, float boundaryX, float boundaryY )
{
	viewport.topLeftX = topLeftX;
	viewport.topLeftY = topLeftY;
	viewport.boundaryX = boundaryX;
	viewport.boundaryY = boundaryY;
}

void Camera::SetActionRange( float topLeftX, float topLeftY, float boundaryX, float boundaryY )
{
	actionRange.topLeftX = topLeftX;
	actionRange.topLeftY = topLeftY;
	actionRange.boundaryX = boundaryX;
	actionRange.boundaryY = boundaryY;
}

void Camera::MoveX( float delta )
{
	viewport.topLeftX += delta;
	viewport.boundaryX += delta;
}

void Camera::MoveY( float delta )
{
	viewport.topLeftY += delta;
	viewport.boundaryY += delta;
}

void Camera::Update()
{
	center.x = ( viewport.topLeftX + viewport.boundaryX ) / 2.0f;
	center.y = ( viewport.topLeftY + viewport.boundaryY ) / 2.0f;

	Vector2D centerOfPlayer;
	centerOfPlayer.x = ( float )actor -> GetDrawable() -> GetRect() -> x;
	centerOfPlayer.y = actor -> GetDrawable() -> GetRect() -> y + 
		actor -> GetDrawable() -> GetRect() -> h / 2.0f;

	if( viewport.boundaryX + centerOfPlayer.x - center.x < actionRange.boundaryX &&
		viewport.topLeftX + centerOfPlayer.x - center.x > actionRange.topLeftX )
	{
		MoveX( centerOfPlayer.x - center.x );
	}

	if( viewport.boundaryY + centerOfPlayer.y - center.y < actionRange.boundaryY &&
		viewport.topLeftY + centerOfPlayer.y - center.y > actionRange.topLeftY )
	{
		MoveY( centerOfPlayer.y - center.y );
	}
}

void Camera::Follow( Actor* actor)
{
	this -> actor = actor;
}

AABB Camera::GetViewport()
{
	return viewport;
}