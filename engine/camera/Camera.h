#pragma once

#include "../structures/AABB.h"
#include "../scene/Actor.h"

class Camera
{

private:

	AABB viewport;			// Defines the viewport of the camera
	AABB actionRange;		// Defines the action range of the camera

	Vector2D center;		// Center of the camera

	Actor* actor;			// Actor to be followed

public:

	Camera();
	~Camera();

	// Set the viewport of the camera
	void SetViewport( float topLeftX, float topLeftY, float boundaryX, float boundaryY );

	// Set the action range of the camera
	void SetActionRange( float topLeftX, float topLeftY, float boundaryX, float boundaryY );

	// Move the camera
	void MoveX( float delta );
	void MoveY( float delta );

	// Get Camera viewport
	AABB GetViewport();

	// Follow an actor
	void Follow( Actor* actor );

	// Update camera position
	void Update();
};

