#pragma once

class Random
{
private:

	Random();
	static Random* instance;

public:

	static Random* GetInstance();
	float GetRandom( int range );							// Get random float from 0 to range
	float GetRandom( float from, float to );				// Get random float range in [from,to)

	// Get random float range in [from,to), every two result no larger than step
	float GetRandom( float from, float to, float step );	
};