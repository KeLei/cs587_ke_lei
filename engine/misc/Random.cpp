#include "Random.h"
#include <time.h>
#include <iostream>

Random* Random::instance = NULL;

Random::Random()
{
	srand( ( unsigned ) time(0) );
}

Random* Random::GetInstance()
{
	if( instance == NULL )
	{
		instance = new Random;
	}
	return instance;
}


float Random::GetRandom( int range )
{
	return ( float )( rand() % RAND_MAX ) / ( float ) RAND_MAX * ( float )range;
}

float Random::GetRandom( float from, float to )
{
	float range = to - from;
	return ( float )( rand() % RAND_MAX ) / ( float ) RAND_MAX * ( float )range + from;
}

float Random::GetRandom( float from, float to, float step )
{
	return 0;
}
