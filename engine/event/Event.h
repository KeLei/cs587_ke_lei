#pragma once

#include "../scene/Actor.h"

enum EventType
{
	/*
	* TODO: read this through json
	*/
	COLLISITON,

};


class Event
{
private:

	Actor* trigger;
	Actor* taker;

	int eventType;

public:

	Event( Actor* trigger, Actor* taker, EventType type )
	{
		eventType = type;
		this -> trigger = trigger;
		this -> taker = taker;
	}

	int GetEventType()
	{
		return eventType;
	}

	Actor* GetTrigger()
	{
		return trigger; 
	}

	Actor* GetTaker()
	{
		return taker; 
	}

};