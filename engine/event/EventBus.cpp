#include "EventBus.h"

EventBus* EventBus::eventBusInstance = NULL;

EventBus* EventBus::GetInstance()
{
	if( eventBusInstance == NULL )
	{
		eventBusInstance = new EventBus;
	}
	return eventBusInstance;
}

void EventBus::NotifySubscribers( Event eventType )
{
	for( int i = 0; i < subscribers.Size(); i++ )
	{
		subscribers[ i ] -> Update( eventType );
	}
}

void EventBus::AddSubscriber( IEventSubScriber* subscriber )
{
	subscribers.PushBack( subscriber );
}

