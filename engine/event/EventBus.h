#pragma once 

#include "../structures/DynamicArray.h"
#include "../interface/IEventSubscriber.h"
#include "../event/Event.h"

class EventBus
{

private:

	static EventBus* eventBusInstance;
	EventBus(){}

public:

	DynamicArray< int > eventList;
	DynamicArray< IEventSubScriber* > subscribers;

	static EventBus* GetInstance();

	void NotifySubscribers( Event eventType );
	void AddSubscriber( IEventSubScriber* subscriber );

};

