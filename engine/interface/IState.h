#pragma once

#include <string>
#include "../interface/ITickable.h"

class IState : public ITickable
{
public:

	// Do state logic
	virtual void Tick() = 0;

	// Get the next state
	virtual std::string GetNextState() = 0;

	// Get the current state
	virtual std::string GetCurrentState() = 0;
};