#ifndef _ITICKABLE_H__
#define _ITICKABLE_H__

class ITickable
{

public:

	virtual void Tick() = 0;

};




#endif // _ITICKABLE_H__
