#pragma once
#include "ITickable.h"
#include "IState.h"
#include <string>

class IStateMachine : public ITickable
{
	// Get the current state
	virtual std::string GetCurrentState() = 0;

	// Change state to the state specified
	virtual void ChangeState( std::string stateKey ) = 0;

	// Add a state to the state machine
	virtual void AddState( std::string key, IState* state ) = 0;
};