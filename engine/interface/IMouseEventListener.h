#pragma once

class IMouseEventListener
{
	virtual void OnClicked() = 0;
};