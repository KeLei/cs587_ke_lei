#pragma once

#include "../scene/Actor.h"
#include "../scene/Physics.h"

class ICollisionHandler
{
	virtual void OnCollision( Physics* physicsBeforeCollision, Actor* collisionTaker ) = 0;
};