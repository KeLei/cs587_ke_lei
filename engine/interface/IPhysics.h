#pragma once

#include <SDL.h>
#include "../structures/Vector2D.h"

class IPhysics
{
public:

	// Set object speed
	virtual void SetVelocity( Vector2D velocity ) = 0;

	// Set object coordinate
	virtual void SetPosition( Vector2D coordinate ) = 0;

	// Set object gravity
	virtual void SetGravity( Vector2D gravity ) = 0;

	// Set object speed
	virtual void SetXVelocity( float velocityX ) = 0;

	// Set object speed
	virtual void SetYVelocity( float velocityY ) = 0;

	// Set object acceleration speed
	virtual void SetXAcceleration( float acceleration ) = 0;

	// Set object acceleration speed
	virtual void SetYAcceleration( float acceleration ) = 0;

	// Set object x coordinate
	virtual void SetXCoordinate( float coordinateX ) = 0;

	// Set object y coordinate
	virtual void SetYCoordinate( float coordinateY ) = 0;

	// Set object y coordinate
	virtual void SetMaxSpeed( float maxSpeed ) = 0;

	// Set object coordinate, speed 
	virtual void SetPhysics( SDL_Rect* rectangle, Vector2D velocity ) = 0;

	// Set up whether the object is collidable
	virtual void SetCollidable() = 0;
	virtual void SetIncollidable() = 0;

	virtual void Move() = 0;
	virtual void Move( Vector2D delta ) = 0;

	// Get object speed
	virtual Vector2D& GetVelocity() = 0;

	// Get object speed
	virtual Vector2D& GetPosition() = 0;

	// Get gravity
	virtual Vector2D GetGravity() = 0;

	// Get current x-coordinate acceleration
	virtual float GetXAcceleration() = 0;

	// Get current y-coordinate acceleration
	virtual float GetYAcceleration() = 0;

	// Get object x bound
	virtual float GetXBoundary() = 0;

	// Get object y bound
	virtual float GetYBoundary() = 0;

	// Set object y coordinate
	virtual float GetMaxSpeed() = 0;

	// If the object is collidable
	virtual bool IsCollidable() = 0;			

	// If the object collide with other object
	virtual bool IsCollideWith( IPhysics* actorPhysics ) = 0;
};
