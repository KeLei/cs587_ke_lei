#pragma once

class ITimeout
{
	virtual void OnTimeout() = 0;
};