#pragma once

#include "../event/Event.h"

class IEventSubScriber
{

public:
	// Update function interface
	virtual void Update( Event eventType) = 0;

};