#include "JsonValue.h"

JsonValue::JsonValue():
	type( UNDEFINED )
{
}

JsonValue::JsonValue( float value ):
	numberValue( value ),
	type( NUMBER )
{
}

JsonValue::JsonValue( string value ):
	stringValue( value ),
	type( STRING )
{
}

JsonValue::JsonValue( bool value ):
	booleanValue( value ),
	type( BOOLEAN )
{
}

JsonValue::JsonValue( DynamicArray< JsonValue* > value ):
	valueArray( value ),
	type( ARRAY )
{
}

JsonValue::JsonValue( HashMap< JsonValue* > value ):
	valueMap( value ),
	type( JSON_OBJECT )
{
}

JsonValue::~JsonValue()
{
}

JsonType JsonValue::GetType()
{
	return type;
}

float JsonValue::GetNumberValue()
{
	assert( type == NUMBER );
	return numberValue;
}

bool JsonValue::GetBooleanValue()
{
	assert( type == BOOLEAN );
	return booleanValue;
}

string JsonValue::GetStringValue()
{
	assert( type == STRING );
	return stringValue;
}

DynamicArray< JsonValue* >& JsonValue::GetValueArray()
{
	assert( type == ARRAY );
	return valueArray;
}

HashMap< JsonValue* >& JsonValue::GetValueMap()
{
	assert( type == JSON_OBJECT );
	return valueMap;
}

void JsonValue::operator= ( JsonValue& valueObj )
{
	switch( valueObj.type )
	{
	case UNDEFINED : break;
	case NUMBER : numberValue = valueObj.numberValue; break;
	case STRING: stringValue = valueObj.stringValue; break;
	case BOOLEAN : booleanValue = valueObj.booleanValue; break;
	case ARRAY : valueArray = valueObj.valueArray; break;
	case JSON_OBJECT: valueMap = valueObj.valueMap; break;
	}
}

JsonValue& JsonValue::operator[] ( int index )
{
	assert( type == ARRAY );
	return *( valueArray[ index ] );
}

JsonValue& JsonValue::operator[] ( string key )
{
	assert( type == JSON_OBJECT );
	return *( valueMap[ key ] );
}