#pragma once

#include <string>
#include <cassert>
#include "../structures/DynamicArray.h"
#include "../structures/HashMap.h"

using namespace std;

enum JsonType
{
	UNDEFINED,
	NUMBER,
	STRING,
	BOOLEAN,
	ARRAY,
	JSON_OBJECT,
};


class JsonValue
{

private:

	JsonType type;							// Type of this Json object

	float numberValue;						// value is a number
	bool booleanValue;						// value is a boolean
	string stringValue;						// value is a string
	DynamicArray< JsonValue* > valueArray;	// value is an array
	HashMap< JsonValue* > valueMap;			// value is a json object

public:

	JsonValue();
	JsonValue( float value );
	JsonValue( bool value );
	JsonValue( string value );
	JsonValue( DynamicArray< JsonValue* > value );
	JsonValue( HashMap< JsonValue* > value  );

	JsonType GetType();
	float GetNumberValue();
	bool GetBooleanValue();
	string GetStringValue();
	DynamicArray< JsonValue* >& GetValueArray();
	HashMap< JsonValue* >& GetValueMap();

	void operator= ( JsonValue& valueObj );
	JsonValue& operator[] ( int index );
	JsonValue& operator[] ( string key );

	~JsonValue();
};

