#include "JsonParser.h"
#include <cassert>


JsonParser::JsonParser()
{
}

JsonParser::JsonParser( const string& filePath )
{
	jsonFile = ifstream( filePath );
}

JsonParser::~JsonParser()
{
}

JsonValue* JsonParser::Parse()
{
	assert( jsonFile.is_open() == true );

	return ParseJsonObject();
}

void JsonParser::MoveToNextValidSymbol()
{
	char symbol;
	do
	{
		symbol = jsonFile.get();
	}
	while( symbol == ' ' ||
		symbol == '\n' ||
		symbol == '\t' ||
		symbol == '\r' );
	jsonFile.unget();
}

JsonValue* JsonParser::ParseJsonObject()
{
	MoveToNextValidSymbol();

	char symbol = jsonFile.get();
	assert( symbol == '{' );
	HashMap< JsonValue* > jsonObj;

	while ( true )
	{
		MoveToNextValidSymbol();
		symbol = jsonFile.get();
		assert( symbol == '\"' );
		string key = GetNextString();

		MoveToNextValidSymbol();
		symbol = jsonFile.get();
		assert( symbol == ':' );

		MoveToNextValidSymbol();
		symbol = jsonFile.get();
		switch( symbol )
		{
			// value is a string
		case '\"' :	jsonObj.Insert( key, ParseJsonString() ); break;

			// value is a array
		case '[' :	jsonObj.Insert( key, ParseJsonArray() ); break;

			// value is a bool
		case 't' :
		case 'f' :	jsonObj.Insert( key, ParseJsonBool() ); break;

			// value is a number
		case '-' :
		case '0' :
		case '1' :
		case '2' :
		case '3' :
		case '4' :
		case '5' :
		case '6' :
		case '7' :
		case '8' :
		case '9' : jsonObj.Insert( key, ParseJsonNumber() ); break;

			// value is a json object
		case '{' : jsonFile.unget();
			jsonObj.Insert( key, ParseJsonObject() ); break;

			// invalid format
		default : assert( false ); break;
		}

		MoveToNextValidSymbol();
		symbol = jsonFile.get();
		if( symbol == ',' )
		{
			continue;
		}
		else if( symbol == '}' )
		{
			return new JsonValue( jsonObj );
		}
		assert( false );
	}
	return NULL;
}

string JsonParser::GetNextString()
{
	string key;
	char symbol = jsonFile.get();
	do
	{
		key += symbol;
		symbol = jsonFile.get();
	}while ( symbol != '\"');
	return key;
}

JsonValue* JsonParser::ParseJsonNumber()
{
	jsonFile.unget();
	string value;
	char symbol = jsonFile.get();
	do
	{
		value += symbol;
		symbol = jsonFile.get();
	}while ( isdigit( symbol ) || symbol == '.' );
	jsonFile.unget();
	return new JsonValue( ( float )atof( value.c_str() ) );
}

JsonValue* JsonParser::ParseJsonBool()
{
	jsonFile.unget();
	if( jsonFile.get() == 't' )
	{
		return new JsonValue( true );
	}
	else
	{
		return new JsonValue( false );
	}
}

JsonValue* JsonParser::ParseJsonString()
{
	string value = GetNextString();

	return new JsonValue( value );
}

JsonValue* JsonParser::ParseJsonArray()
{
	char symbol;
	bool endOfArray = false;
	DynamicArray< JsonValue* > jsonObj;

	while( !endOfArray )
	{
		MoveToNextValidSymbol();
		symbol = jsonFile.get();
		switch( symbol )
		{
			// value is a string
		case '\"' :	jsonObj.PushBack( ParseJsonString() ); break;

			// value is a array
		case '[' :	jsonObj.PushBack( ParseJsonArray() ); break;

			// value is a bool
		case 't' :
		case 'f' :	jsonObj.PushBack( ParseJsonBool() ); break;

			// value is a number
		case '-' :
		case '0' :
		case '1' :
		case '2' :
		case '3' :
		case '4' :
		case '5' :
		case '6' :
		case '7' :
		case '8' :
		case '9' : jsonObj.PushBack( ParseJsonNumber() ); break;

			// value is a json object
		case '{' : jsonObj.PushBack( ParseJsonObject() ); break;

			// invalid format
		default: assert( false ); break;
		}

		MoveToNextValidSymbol();
		symbol = jsonFile.get();
		if( symbol == ',' )
		{
			continue;
		}
		else if( symbol == ']' )
		{
			// End of array
			return new JsonValue( jsonObj );
		}

		// Format error
		assert( false );
	}
	return NULL;
}
