#pragma once

#include <fstream>
#include <string>
#include "JsonValue.h"

using namespace std;

class JsonParser
{
private:

	ifstream jsonFile;

	void MoveToNextValidSymbol();

	string GetNextString();

	JsonValue* ParseJsonNumber();
	JsonValue* ParseJsonBool();
	JsonValue* ParseJsonString();
	JsonValue* ParseJsonArray();
	JsonValue* ParseJsonObject();

public:

	JsonParser();
	JsonParser( const string& filePath );

	JsonValue* Parse();

	~JsonParser();
};