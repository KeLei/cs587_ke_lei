#include "ResourceManager.h"

ResourceManager* ResourceManager::instance = nullptr;

ResourceManager::ResourceManager()
{
	resourceObj = NULL;
}


ResourceManager::~ResourceManager()
{
}

ResourceManager* ResourceManager::GetInstance()
{
	if( instance == nullptr )
	{
		instance = new ResourceManager;
	}
	return instance;
}

void ResourceManager::Initialize( SDL_Renderer* renderer )
{
	this -> renderer = renderer;
}

void ResourceManager::UseResource( char* resourceJsonFilePath )
{
	if( resourceObj != NULL )
	{
		// Discard previous json object and related resources
		FreeResource();
		delete resourceObj;
	}

	JsonParser parser( resourceJsonFilePath );
	resourceObj = parser.Parse();
}

void ResourceManager::LoadTexture()
{
	for( int i = 0; i < (*resourceObj)[ "textures" ].GetValueArray().Size(); i++ )
	{
		textureMap.Insert(
			( ( (*resourceObj)[ "textures" ].GetValueArray())[i] -> GetValueArray() )[0] -> GetStringValue(),
			IMG_LoadTexture( renderer, ( ( ( (*resourceObj)[ "textures" ].GetValueArray())[i]->GetValueArray() )[1] -> GetStringValue() ).c_str() ) 
			);
	}
}

void ResourceManager::LoadActor()
{
	for( int i = 0; i < (*resourceObj)[ "actors" ].GetValueArray().Size(); i++ )
	{
		JsonParser actorParser( ( ( (*resourceObj)[ "actors" ].GetValueArray())[i] -> GetValueArray() )[1] -> GetStringValue() );
		actorMap.Insert(
			( ( (*resourceObj)[ "actors" ].GetValueArray())[i] -> GetValueArray() )[0] -> GetStringValue(),
			actorParser.Parse() );
	}
}

void ResourceManager::LoadFont()
{
	for( int i = 0; i < (*resourceObj)[ "fonts" ].GetValueArray().Size(); i++ )
	{
		fontMap.Insert(
			( ( (*resourceObj)[ "fonts" ].GetValueArray())[i] -> GetValueArray() )[0] -> GetStringValue(),
			TTF_OpenFont( ( ( ( (*resourceObj)[ "fonts" ].GetValueArray())[i] -> GetValueArray() )[1] -> GetStringValue() ).c_str(),
			( int )( ( ( (*resourceObj)[ "fonts" ].GetValueArray())[i] -> GetValueArray() )[2] -> GetNumberValue() ) ) );
	}
}

void ResourceManager::DeleteFont( string key )
{
	TTF_CloseFont( fontMap[ key ] );
	fontMap.Erase( key );
}

SDL_Texture* ResourceManager::GetTexture( string key )
{
	return textureMap[ key ];
}

TTF_Font* ResourceManager::GetFont( string key )
{
	return fontMap[ key ];
}

JsonValue* ResourceManager::GetActorConfig( string key )
{
	return actorMap[ key ];
}

void ResourceManager::FreeResource()
{
	for( int i = 0; i < (*resourceObj)[ "textures" ].GetValueArray().Size(); i++ )
	{
		SDL_DestroyTexture( textureMap[ ( ( (*resourceObj)[ "textures" ].GetValueArray())[i]->GetValueArray() )[0] -> GetStringValue() ]);
	}

	for( int i = 0; i < (*resourceObj)[ "actors" ].GetValueArray().Size(); i++ )
	{
		delete actorMap[ ( ( (*resourceObj)[ "actors" ].GetValueArray())[i]->GetValueArray() )[0] -> GetStringValue() ];
	}

	for( int i = 0; i < (*resourceObj)[ "fonts" ].GetValueArray().Size(); i++ )
	{
		TTF_CloseFont( fontMap[ ( ( (*resourceObj)[ "fonts" ].GetValueArray())[i]->GetValueArray() )[0] -> GetStringValue() ] );
	}

	textureMap.Clear();
	actorMap.Clear();
	fontMap.Clear();
}