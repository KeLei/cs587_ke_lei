#include "Text.h"


Text::Text()
{
	drawable = new Drawable;
}


Text::~Text()
{
	delete drawable;
}

void Text::Setup( std::string text, int positionX, int positionY, TTF_Font* font, SDL_Color color, SDL_Renderer* renderer )
{
	// 
	int width = 0;
	int height = 0;

	SDL_Surface* surface = TTF_RenderText_Blended(font, text.c_str(), color);
	drawable -> SetRenderer( renderer );

	SDL_Texture* texture = SDL_CreateTextureFromSurface( renderer, surface );
	// Get texture size
	SDL_QueryTexture( texture, NULL, NULL, &width, &height );

	drawable -> SetTexture( texture );
	drawable -> GetRect() -> x = positionX;
	drawable -> GetRect() -> y = positionY;
	drawable -> GetRect() -> w = width;
	drawable -> GetRect() -> h = height;
}

void Text::Draw()
{
	drawable -> Draw( drawable -> GetRenderer(), 0, 0, 1.0f );
}