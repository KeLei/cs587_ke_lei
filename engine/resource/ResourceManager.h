#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include "../json/JsonParser.h"
#include "Text.h"

/*
* Resource Manager: singleton , manages all resources in the game 
*/

class ResourceManager
{

private:

	static ResourceManager* instance;		// Instance of the manager
	SDL_Renderer* renderer;					// Render context

	HashMap< SDL_Texture* > textureMap;		// Texture resource
	HashMap< JsonValue* > actorMap;			// Actor resource
	HashMap< TTF_Font* > fontMap;			// Font resource

	JsonValue* resourceObj;					// Json object of the resource file
	ResourceManager();

public:

	// Get manager instance
	static ResourceManager* GetInstance();

	// Initialize manager with renderer
	void Initialize( SDL_Renderer* renderer );

	// Load resources through a json file
	void UseResource( char* resourceJsonFilePath );

	// Load textures from the json
	void LoadTexture();

	// Load actor configuration from json
	void LoadActor();

	// Load actor configuration from json
	void LoadFont();

	// Load actor configuration from json
	void DeleteFont( string key );

	// Get a texture based on the string key
	SDL_Texture* GetTexture( string key );

	// Get a font based on the string key
	TTF_Font* GetFont( string key );

	// Get actor configuresion json from the json
	JsonValue* GetActorConfig( string key );

	// Free current resources
	void FreeResource();

	~ResourceManager();
};

