#pragma once

#include <SDL_ttf.h>
#include <string>

#include "../scene/Drawable.h"

class Text
{

private:

	std::string text;		// Text content of the text object
	Drawable* drawable;		// Drawable part

public:

	Text();
	~Text();

	// Setup this text, include color, position and font
	void Setup( std::string text, 
		int positionX, 
		int positionY, 
		TTF_Font* font, 
		SDL_Color color,
		SDL_Renderer* renderer );

	// Draw text
	void Draw();

};

