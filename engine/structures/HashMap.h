#ifndef HASHMAP_H_INCLUDED
#define HASHMAP_H_INCLUDED

#include <iostream>
#include <cassert>
#include "DynamicArray.h"
#include "HashNode.h"
#include <string>

using namespace std;


//HashMap class
template < class T >
class HashMap
{

private:

	DynamicArray< HashNode< T > > nodes;
	

public:

	HashMap();
	HashMap( HashMap & hashmapObj );

	~HashMap();

	//calculate hash code for each string key
	int HashCode( string key );

	//access to string for insert and query
	T& operator[] ( string key );

	//access to string for insert and query
	void operator= ( HashMap< T >& mapObj );

	//insert elements key and value
	void Insert( string key, T value );

	//delete key
	void Erase( string key );

	//delete all elements
	void Clear();
};

template < class T >
HashMap< T >::HashMap()
{
	nodes.Resize( 100 );
}

template < class T >
HashMap< T >::HashMap( HashMap & hashmapObj )
{
	nodes = hashmapObj.nodes;
}

template < class T >
HashMap< T >::~HashMap()
{
	nodes.Clear();
}

template <class T>
T& HashMap< T >::operator[] ( string key )
{
	int index = HashCode( key ) % 100;

	if ( nodes[ index ].value.Size() == 0 )
	{
		//if the original size 
		nodes[ index ].value.Resize( 1 );
		nodes[ index ].key.PushBack( key );

		return nodes[ index ].value[ 0 ];
	}
	else 
	{
		int collisionPosition;
		if( ( collisionPosition = nodes[ index ].SearchString( key ) ) >= 0 )
		{
			//return the collision value
			return nodes[ index ].value[ collisionPosition ];
		}
		else //add one more value
		{
			//resize the array to add more
			nodes[ index ].value.Resize( nodes[ index ].value.Size() + 1 );
			nodes[ index ].key.PushBack( key );

			return nodes[ index ].value[ nodes[ index ].value.Size() - 1 ];
		}
	}
}

template <class T>
void HashMap< T >::operator= ( HashMap< T >& mapObj )
{
	nodes = mapObj.nodes;
}

template <class T>
int HashMap< T >::HashCode( string key )
{
	int sum = 0;
	for( unsigned int i = 0; i < key.length(); i++ )
	{
		sum += ( int )( key[ i ] ) - ' ';
	}

	return sum / key.length();
}

template <class T>
void HashMap< T >::Insert( string key, T value )
{
	( *this )[ key ] = value;
	return;
}

template <class T>
void HashMap< T >::Erase( string key )
{
	int index = HashCode( key ) % 100;
	int searchPosition;
	assert( ( searchPosition = nodes[ index ].SearchString( key ) ) >= 0 );

	nodes[ index ].value.Erase( searchPosition );
	nodes[ index ].key.Erase( searchPosition );

	return;
}

template <class T>
void HashMap< T >::Clear()
{
	nodes.Clear();
}



#endif