#pragma once

#include <cassert>
#include <iostream>

template <class T>
class DynamicArray
{


private:

	int arraySize;				//size of the array
	int arrayCapacity;			//capacity of the array
	T* dynamicArray;			//the array pointer


public:

	DynamicArray();
	DynamicArray( int arraySize );
	DynamicArray( T* originArray, int arraySize);
	DynamicArray( DynamicArray< T > &arrayObj);

	~DynamicArray();

	//Get Size of the array
	int Size();

	//Get Capacity of the array
	int Capacity();

	//whether the array is empty
	bool IsEmpty();

	//Resize the array, add 0 if Size is larger; delete element id Size is smaller
	void Resize( int newSize );

	//Reserve space for more elements
	void Reserve( int newReserve );

	//Assign value to specified index of array
	void Assign( int index, T element );

	//add new element to the end of array
	void PushBack( T newElement );

	//add new element to the front of array
	void PushFront( T newElement );

	//pop out the last element of the array
	T PopBack();

	//Insert element into specified index
	void Insert( int index, T value );

	//Erase element from specified index
	void Erase( int index );

	//Erase element from ranging from fIndex to bIndex
	void Erase( int fIndex, int bIndex );

	//Clear all elements in array
	void Clear();

	T at( int index );

	//assignment operator
	T& operator[] ( int index );

	//access operator
	T operator[] ( int index ) const;

	void operator= ( DynamicArray< T > & dynamicArrayOb );

};



template <class T>
DynamicArray< T >::DynamicArray( )
{
	arraySize = arrayCapacity = 0;
	dynamicArray = NULL;
}

template <class T>
DynamicArray< T >::DynamicArray( int v_size )
{
	arraySize = arrayCapacity = v_size;
	dynamicArray = new T[ arrayCapacity ];
	for(int i = 0; i < arraySize; i++)
	{
		dynamicArray[i] = 0;
	}
}

template <class T>
DynamicArray< T >::DynamicArray( T* originArray, int arraySize )
{
	arraySize = arrayCapacity = arraySize;
	dynamicArray = new T[ arrayCapacity ];
	for(int i = 0; i < arraySize; i++)
	{
		dynamicArray[ i ] = originArray[ i ];
	}
}

template <class T>
DynamicArray< T >::DynamicArray( DynamicArray< T > & arrayObj)
{
	arraySize = arrayCapacity = arrayObj.Size();
	dynamicArray = new T[ arrayCapacity ];
	for(int i = 0; i < arraySize; i++)
	{
		dynamicArray[ i ] = arrayObj.dynamicArray[ i ];
	}
}

template <class T>
int DynamicArray< T >::Size()
{
	return arraySize;
}

template <class T>
int DynamicArray< T >::Capacity()
{
	return arrayCapacity;
}

template <class T>
bool DynamicArray< T >::IsEmpty()
{
	if( arraySize == 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

template <class T>
void DynamicArray< T >::Resize( int newSize )
{
	assert( newSize >= 0);
	if( newSize != arraySize )
	{
		T* oldArray = dynamicArray;
		arrayCapacity = ( arrayCapacity > newSize ) ? arrayCapacity : newSize ;
		dynamicArray = new T[ arrayCapacity ];
		for(int i = 0; i < newSize; i++)
		{
			if(i < arraySize)
			{
				dynamicArray[ i ] = oldArray[ i ];
			}
		}
		delete []oldArray; 
		arraySize = newSize;
	}	
}

template <class T>
void DynamicArray< T >::Reserve( int newReserve )
{
	assert( newReserve >= arraySize );
	if ( newReserve != arrayCapacity )
	{
		T* oldArray = dynamicArray;
		dynamicArray = new T[ newReserve ];
		for(int i = 0; i < arraySize; i++)
			dynamicArray[ i ] = oldArray[ i ];
		arrayCapacity = newReserve;
		delete []oldArray;
	}
}

template <class T>
void DynamicArray< T >::Assign(int index, T element)
{
	assert( index >= 0 && index <= arraySize );
	dynamicArray[ index ] = element; 
}

template <class T>
void DynamicArray< T >::PushBack(T newElement)
{
	if ( arrayCapacity == arraySize )
	{
		Reserve( arrayCapacity * 2 + 1);
	}
	arraySize++ ;
	dynamicArray[ arraySize- 1 ] = newElement;
}

template <class T>
void DynamicArray< T >::PushFront(T newElement)
{
	if ( arrayCapacity == arraySize )
	{
		Reserve( arrayCapacity * 2 + 1);
	}
	arraySize++ ;

	T* oldArray = dynamicArray;
	dynamicArray = new T[ arrayCapacity ];

	for (int i = 1; i < arraySize; i++)
	{
		dynamicArray[ i ] = oldArray[ i - 1 ];
	}

	dynamicArray[ 0 ] = newElement;

	delete []oldArray;
}


template <class T>
T DynamicArray< T >::PopBack()
{
	assert( arraySize > 0 );
	T temp = dynamicArray[ arraySize - 1 ];
	Resize( --arraySize );
	return temp; 
}

template <class T>
void DynamicArray< T >::Insert(int index, T value)
{
	assert( index <= arraySize && index >= 0 );
	arraySize++ ;
	for( int i = arraySize - 1; i > index; i-- )
	{
		dynamicArray[ i ] = dynamicArray[ i - 1 ];
	}
	dynamicArray[ index ] = value;
}

template <class T>
void DynamicArray< T >::Erase(int index)
{
	assert( index <= arraySize && index >= 0 );
	T* oldArray = dynamicArray;
	arraySize-- ;
	dynamicArray = new T[ arrayCapacity ];
	for(int i = 0; i < index; i++)
	{
		dynamicArray[ i ] = oldArray[ i ];
	}
	for(int i = index; i < arraySize; i++)
	{
		dynamicArray[ i ] =  oldArray[ i + 1 ];
	}
	delete []oldArray;
}

template <class T>
void DynamicArray< T >::Erase(int fIndex, int bIndex)
{
	assert(fIndex >= 0 && bIndex <= arraySize && fIndex <= bIndex);
	T* oldArray = dynamicArray;
	arraySize = arraySize - ( bIndex - fIndex );
	dynamicArray = new T[ arrayCapacity ];
	for(int i = 0; i < fIndex; i++)
	{
		dynamicArray[ i ] =  oldArray[ i ];
	}
	for(int i = fIndex; i < arraySize; i++)
	{
		dynamicArray[ i ] =  oldArray[ i + bIndex - fIndex ];
	}
	delete []oldArray;
}

template <class T>
void DynamicArray< T >::Clear()
{
	Resize(0);
}

template <class T>
T DynamicArray< T >::at( int index )
{
	assert( index <= arraySize && index >= 0 );
	return dynamicArray[ index ];
}

template <class T>
T& DynamicArray< T >::operator[] ( int index )
{
	assert( index <= arraySize && index >= 0 );
	return dynamicArray[ index ];
}

template <class T>
T DynamicArray< T >::operator[] ( int index )const
{
	assert( index <= arraySize && index >= 0 );
	return dynamicArray[ index ];
}

template <class T>
void DynamicArray< T >::operator= (DynamicArray< T > & ArrayOb)
{
	delete []dynamicArray;
	arraySize = ArrayOb.Size();
	arrayCapacity = ArrayOb.Capacity();
	dynamicArray = new T[ arrayCapacity ];
	for(int i = 0; i < arraySize; i++)
	{
		dynamicArray[ i ] = ArrayOb.dynamicArray[ i ];
	}
}

template <class T>
DynamicArray< T >::~DynamicArray()
{
	delete []dynamicArray;
}