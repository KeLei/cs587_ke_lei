#pragma once

class AABB
{
public:
	float topLeftX;
	float topLeftY;

	float boundaryX;
	float boundaryY;

	AABB()
	{
		topLeftX = 0;
		topLeftY = 0;
		boundaryX = 0;
		boundaryY = 0;
	}
	~AABB()
	{}

	void operator=( AABB& obj )
	{
		topLeftX = obj.topLeftX;
		topLeftY = obj.topLeftY;
		boundaryX = obj.boundaryX;
		boundaryY = obj.boundaryY;
	}
};