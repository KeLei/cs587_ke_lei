#include "vector2D.h"
#include "math.h"

Vector2D::Vector2D()
{
	x = 0;
	y = 0;
}

Vector2D::Vector2D( float x, float y )
{
	this -> x = x;
	this -> y = y;
}

Vector2D::~Vector2D()
{}

Vector2D Vector2D::operator+( Vector2D rhsObj )
{
	return Vector2D( x + rhsObj.x, y + rhsObj.y );
}

Vector2D Vector2D::operator-( Vector2D rhsObj )
{
	return Vector2D( x - rhsObj.x, y - rhsObj.y );
}

void Vector2D::operator=( Vector2D& vecObj )
{
	x = vecObj.x;
	y = vecObj.y;
}

float Vector2D::NormalLength()
{
	return sqrt( x * x + y * y );
}

void Vector2D::Rotate( float angle )
{}

void Vector2D::Normalize()
{
	if ( x == 0.0 )
	{
		y = 1.0;
	}
	else
	{
		float k = y / x;
		x = sqrt( 1.0 / 1.0 + k * k );
		y = k * x;
	}
}

void Vector2D::SetVector2D( float x, float y )
{
	this -> x = x;
	this -> y = y;
}

float Vector2D::CrossProduct( Vector2D vecObj )
{
	return (x * vecObj.y - vecObj.x * y );
}

float Vector2D::AngleTo( Vector2D vecObj )
{
	return asin( this -> CrossProduct( vecObj ) / ( this -> NormalLength() * vecObj.NormalLength()));
}