
#ifndef HASHNODE_H_INCLUDED
#define HASHNODE_H_INCLUDED

#include <string>

using namespace std;

//Node class for each position of hashmap
template < class T >
class HashNode
{
public:
	DynamicArray< T > value;				//Array of value
	DynamicArray< string > key;				//Array of key

	HashNode();
	~HashNode();

	//search key in array, return -1 if not found
	int SearchString( string searchKey );

	//Assignment operator
	void operator= ( HashNode< T > &rhs );
};

template < class T >
HashNode< T >::HashNode(){}

template < class T >
HashNode< T >::~HashNode(){}

template < class T >
void HashNode< T >::operator= ( HashNode< T > &rhs  )
{
	value = rhs.value;
	key = rhs.key;
}

template < class T >
int HashNode< T >::SearchString ( string searchKey )
{
	for (int i = 0; i < key.Size() ; i++)
	{
		if( key[ i ] == searchKey )
			return i;
	}

	return -1 ;
}

#endif
