#pragma once

class Vector2D
{

public:

	// x and y coordinate of the vector
	float x;
	float y;

	Vector2D();
	Vector2D( float x, float y );
	~Vector2D();

	void Rotate( float angle );
	void Normalize();
	void SetVector2D( float x, float y );

	Vector2D operator+ ( Vector2D rhsVec );
	Vector2D operator- ( Vector2D rhsVec );
	void operator= ( Vector2D& vecObj );

	float NormalLength();
	float CrossProduct( Vector2D vecObj );
	float AngleTo( Vector2D vecObj );
};