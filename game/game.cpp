#include "Game.h"

Game::Game()
{
	// Initialize paraments
	window = NULL;
	isRunning = true;
	isPaused = false;

	frameTimer.SetInterval( ( int )( 1000.0 / MAX_FPS ) );
	screenSizeH = 640;
	screenSizeW = 800;
}

void Game::Execute()
{

	if( Init() == false )
	{
		// Initialize fail
		return;
	}

	if( LoadContent() == false )
	{
		// Load image fail
		return;
	}


	while( isRunning )
	{
		frameTimer.RestartTimer();

		if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_SPACE ] == KEY_JUST_PRESSED )
		{
			isPaused = !isPaused;
		}
		// Handle events
		if( !EventLoop() )
		{
			// Quit game
			break;
		}
		if ( !isPaused )
		{
			Render();		// Do drawing
		}

		if( !frameTimer.IsTimeout() )
		{
			// If reach maxinum frame rate
			Uint32 delayTime = ( Uint32 )( ( 1000.0 / MAX_FPS - frameTimer.GetElapsedTime() ) / 10.0 );
			SDL_Delay( delayTime );
		}
	}

	Cleanup();			// Clean up everything

	return;
}

bool Game::Init()
{
	if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
	{
		return false;
	}

	if( TTF_Init() == -1 )
	{
		return false;
	}

	// Initialize window
	if( ( window = SDL_CreateWindow( "SDL", 100, 100, screenSizeW, screenSizeH, SDL_RENDERER_PRESENTVSYNC  ) ) == NULL)
	{
		return false;
	}

	// Setup renderer
	renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
	if( renderer == NULL )
	{
		cout << SDL_GetError() << endl;
		return 1;
	}

	// Set renderer of the scene
	SceneManager::GetInstance() -> SetRenderer( renderer );

	// Setup camera
	SceneManager::GetInstance() -> SetCamera( 0.0f, 260.0f, 800.0f, 640.0f, 0.0f, 0.0f, 1600.0f, 900.0f );


	return true;
}

bool Game::EventLoop()
{
	return userEvent.UpdateInput();
}

void Game::Render()
{
	SceneManager::GetInstance() -> RunTicks();
	SceneManager::GetInstance() -> Render();
}

bool Game::LoadContent()
{
	// Load resources
	ResourceManager::GetInstance() -> Initialize( renderer );
	ResourceManager::GetInstance() -> UseResource( "assets/json/resources.json" );
	ResourceManager::GetInstance() -> LoadTexture();
	ResourceManager::GetInstance() -> LoadActor();
	ResourceManager::GetInstance() -> LoadFont();

	SceneManager::GetInstance() -> AddLayer( "layer_background2" );
	SceneManager::GetInstance() -> AddLayer( "layer_background1" );
	SceneManager::GetInstance() -> AddLayer( "mainLayer" );
	SceneManager::GetInstance() -> AddLayer( "layer_foreground1" );

	SceneManager::GetInstance() -> GetLayer( "layer_background2" ) -> SetSpeedToCamera( 0.5f );
	SceneManager::GetInstance() -> GetLayer( "layer_background1" ) -> SetSpeedToCamera( 0.7f );
	SceneManager::GetInstance() -> GetLayer( "layer_foreground1" ) -> SetSpeedToCamera( 1.2f );
	SceneManager::GetInstance() -> SetPrimaryLayer( "mainLayer" );

	minionActor = MinionFactory::GetMinion( renderer );
	minionActorController = new MainActorController( minionActor );
	minionActorController -> ReadConfigFile( "assets/json/controller_config/player.json" );

	banana = ActorFactory::GetActor( "banana1", renderer );
	bananaController = new BananaController( banana );

	// Add the actor and controller to scene
	SceneManager::GetInstance() -> AddToScene( ActorFactory::GetActor( "background1", renderer ), "layer_background1" );
	SceneManager::GetInstance() -> AddToScene( ActorFactory::GetActor( "background2", renderer ), "layer_background2" );
	SceneManager::GetInstance() -> AddToScene( ActorFactory::GetActor( "foreground1", renderer ), "layer_foreground1" );
	SceneManager::GetInstance() -> AddToScene( ActorFactory::GetActor( "ground1", renderer ), "mainLayer" );

	SceneManager::GetInstance() -> AddToScene( minionActor, "mainLayer" );
	SceneManager::GetInstance() -> AddToTickable( minionActorController );

	SceneManager::GetInstance() -> AddToScene( banana, "mainLayer" );
	SceneManager::GetInstance() -> AddToTickable( bananaController );

	SceneManager::GetInstance() -> SetCameraWatcher( minionActor );

	// Test text in the scenemanager
	SDL_Color color = { 0, 0, 0 };
	testText = new Text;
	testText -> Setup( "TTF is cool!", 10, 10, 
		ResourceManager::GetInstance() -> GetFont( "FreeMonoBoldOblique" ),
		color, renderer );

	SceneManager::GetInstance() -> AddText( testText, "mainLayer" );
	
	// Generate map
	MapFactory::GenerateMap( "assets/json/map/level1.json", "mainLayer", renderer );

	// Add event subscribers
	EventBus::GetInstance() -> AddSubscriber( minionActorController );
	EventBus::GetInstance() -> AddSubscriber( bananaController );

	return true;
}

void Game::Cleanup()
{
	SDL_DestroyRenderer( renderer );
	SDL_DestroyWindow( window );
	SDL_Quit();
}