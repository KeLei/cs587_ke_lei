#include "MainActor.h"


MainActor::MainActor() : Actor()
{
	orientation = true;
	lastOrientation = orientation;
}

MainActor::MainActor( Actor* actor ) : Actor( *actor )
{
	orientation = true;
	lastOrientation = orientation;
}

MainActor::MainActor( MainActor* actor ) : Actor( *actor )
{
	orientation = true;
	lastOrientation = orientation;
}

MainActor::~MainActor()
{
}

void MainActor::SetOrientation( bool orientation )
{
	lastOrientation = this -> orientation;
	this -> orientation = orientation;
}

bool MainActor::GetOrientation()
{
	return orientation;
}

bool MainActor::OrientationChanged()
{
	return lastOrientation != orientation;
}
