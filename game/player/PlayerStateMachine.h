#pragma once

#include "../../engine/interface/IStateMachine.h"
#include "../../engine/interface/IState.h"
#include "../../engine/scene/Actor.h"
#include "../../engine/structures/HashMap.h"


/*
*  State machine of the player
*/

class PlayerStateMachine : public IStateMachine
{

private:

	string lastState;					// Last state of player
	string currentState;				// Current state of player
	Actor* actor;					// Current player

	HashMap< IState* > stateMap;	// State map

public:

	PlayerStateMachine();
	PlayerStateMachine( string initialState, Actor* player );
	~PlayerStateMachine();

	string GetCurrentState();
	string GetLastState();
	bool StateChanged();

	void SetActor( Actor* player );
	void operator=( PlayerStateMachine &stateMachine );
	void Tick();

	void ChangeState( std::string );
	void AddState( std::string key, IState* state );
};