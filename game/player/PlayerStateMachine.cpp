#include "PlayerStateMachine.h"
#include "../../engine/input/KeyboardHandler.h"
#include <iostream>
#include <math.h>
#include <string>

PlayerStateMachine::PlayerStateMachine()
{
	currentState = "stopped";
}

PlayerStateMachine::PlayerStateMachine( string initialastState, Actor* actor )
{
	currentState = initialastState;
	this -> actor = actor;
}

PlayerStateMachine::~PlayerStateMachine()
{}

string PlayerStateMachine::GetCurrentState()
{
	return currentState;
}

string PlayerStateMachine::GetLastState()
{
	return lastState;
}

void PlayerStateMachine::operator=( PlayerStateMachine &stateMachine )
{
	currentState = stateMachine.currentState;
}

void PlayerStateMachine::SetActor( Actor* actor )
{
	this -> actor = actor;
}

void PlayerStateMachine::Tick()
{
	stateMap[ currentState ] -> Tick();
	lastState = currentState;
	if( stateMap[ currentState ] -> GetNextState() != "" )
	{
		// Change current state to next state
		std::cout << stateMap[ currentState ] -> GetNextState() << std::endl;
		ChangeState( stateMap[ currentState ] -> GetNextState() );
	}
}

bool PlayerStateMachine::StateChanged()
{
	return ( currentState != lastState );
}

void PlayerStateMachine::ChangeState( std::string nextState )
{
	currentState = nextState;
}

void PlayerStateMachine::AddState( std::string key, IState* state )
{
	stateMap.Insert( key, state );
}
