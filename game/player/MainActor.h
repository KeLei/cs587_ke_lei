#pragma once
#include "../../engine/scene/Actor.h"

class MainActor : public Actor
{
private:

	bool orientation;
	bool lastOrientation;

public:

	MainActor();
	MainActor( Actor* actor );
	MainActor( MainActor* actor );
	~MainActor();

	void SetOrientation( bool orientation );
	bool GetOrientation();
	bool OrientationChanged();
};

