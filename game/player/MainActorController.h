#pragma once

#include "MainActor.h"
#include "../../engine/scene/ActorController.h"
#include "../../engine/scene/SceneManager.h"
#include "../../engine/interface/IEventSubscriber.h"
#include "../../engine/event/Event.h"
#include "../../engine/json/JsonParser.h"
#include "../../engine/input/KeyboardHandler.h"
#include "../../engine/resource/ResourceManager.h"
#include "../player/PlayerStateMachine.h"
#include "../playerStates/JumpState.h"
#include "../playerStates/DoubleJumpState.h"
#include "../playerStates/MovingState.h"
#include "../playerStates/StopState.h"
#include "../playerStates/AcceleratingState.h"
#include <iostream>

class MainActorController : public ActorController
{
private:

	void ChangeTexture();


protected:

	MainActor* mainActor;
	PlayerStateMachine stateMachine;

	float jumpSpeed;
	float acceleration;
	float deceleration;

public:

	MainActorController();
	MainActorController( MainActor* mainActor );
	~MainActorController();

	void operator=( MainActorController & mainActorController );		// Assignment operator overload

	void ReadConfigFile( char* filePath );
	void Jump( float speed );
	void MoveDown( float speed );
	void MoveHorizon( float speed, float acceleration );
	void ChangeActorTexture( char* texturePath );

	void Tick();
	void HandleUserEvent( SDL_Event* incomingEvent );					// Update the actor to deal with event

	PlayerStateMachine* GetStateMachine();

	void Update( Event eventType );
	void OnCollision( Physics* physicsBeforeCollision, Actor* collisionTaker );
};