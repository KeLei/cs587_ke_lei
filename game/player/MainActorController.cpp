#include "MainActorController.h"

MainActorController::MainActorController()
{
}

MainActorController::MainActorController( MainActor* mainActor )
{
	this -> mainActor = mainActor;

	stateMachine.SetActor( mainActor );
	stateMachine.AddState( "stopped", new StopState( mainActor ) );
	stateMachine.AddState( "moving", new MovingState( mainActor ) );
	stateMachine.AddState( "jumping", new JumpState( mainActor ) );
	stateMachine.AddState( "doubleJump", new DoubleJumpState( mainActor ) );
	stateMachine.AddState( "accelerating", new AcceleratingState( mainActor ) );
}

MainActorController::~MainActorController()
{
}

void MainActorController::operator=( MainActorController & mainActorController )
{
	mainActor = mainActorController.mainActor;
	stateMachine = PlayerStateMachine( mainActorController.stateMachine.GetCurrentState(), mainActor );
}

void MainActorController::Jump( float speed )
{
	mainActor -> GetPhysics() -> SetYVelocity( speed );
	return;
}

void MainActorController::MoveDown( float speed )
{
	mainActor -> GetPhysics() -> SetYVelocity( speed );
	return;
}

void MainActorController::MoveHorizon( float speed, float acceleration )
{
	mainActor -> GetPhysics() -> SetXVelocity( speed );
	mainActor -> GetPhysics() -> SetXAcceleration( acceleration );
	return;
}

void MainActorController::ReadConfigFile( char* filePath )
{
	JsonParser parser( filePath );
	JsonValue* jsonObj = parser.Parse();

	jumpSpeed = ( *jsonObj )["jump_speed"].GetNumberValue();
	acceleration = ( *jsonObj )["acceleration"].GetNumberValue();
	deceleration = ( *jsonObj )["deceleration"].GetNumberValue();
}

void MainActorController::Tick()
{
	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_d ] == KEY_PRESSED && 
		KeyboardHandler::GetInstance() -> keyStatus[ SDLK_a ] == KEY_RELEASED )
	{
		if( stateMachine.GetCurrentState() == "jumping" || stateMachine.GetCurrentState() == "doubleJump")
		{
			// IN air move without deceleration
			MoveHorizon( mainActor -> GetPhysics() -> GetMaxSpeed(), 0 ); 
		}
		else
		{
			mainActor -> GetPhysics() -> SetXAcceleration( acceleration );
		}
	}

	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_d ] == KEY_JUST_RELEASED ||
		KeyboardHandler::GetInstance() -> keyStatus[ SDLK_d ] == KEY_RELEASED )
	{
		//if( stateMachine.GetCurrentState() != IN_AIR )
		//{
		//	mainActor -> GetPhysics() -> SetXVelocity( 0 );
		//	mainActor -> GetPhysics() -> SetXAcceleration( 0 );
		//	//std::cout << "key d released" << std::endl;
		//}
		//else
		//{
		//	mainActor -> GetPhysics() -> SetXAcceleration( -( float )DECELERATION );
		//}

		if( mainActor -> GetPhysics() -> GetVelocity().x > 0 && 
			stateMachine.GetCurrentState() != "jumping" &&
			stateMachine.GetCurrentState() != "doubleJump" )
		{
			mainActor -> GetPhysics() -> SetXAcceleration( 0 );
			mainActor -> GetPhysics() -> SetXVelocity( 0 );
		}
	}


	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_a ] == KEY_PRESSED && 
		KeyboardHandler::GetInstance() -> keyStatus[ SDLK_d ] == KEY_RELEASED )
	{
		//std::cout << "key a pressed" << std::endl;
		if( stateMachine.GetCurrentState() == "jumping" || stateMachine.GetCurrentState() == "doubleJump")
		{
			// IN air move without deceleration
			MoveHorizon( - mainActor -> GetPhysics() -> GetMaxSpeed(), 0 ); 
		}
		else
		{
			mainActor -> GetPhysics() -> SetXAcceleration( - acceleration );
		}
	}

	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_a ] == KEY_JUST_RELEASED ||
		KeyboardHandler::GetInstance() -> keyStatus[ SDLK_a ] == KEY_RELEASED )
	{
		//if( stateMachine.GetCurrentState() != IN_AIR )
		//{
		//	//std::cout << "key a released" << std::endl;
		//	mainActor -> GetPhysics() -> SetXVelocity( 0 );
		//	mainActor -> GetPhysics() -> SetXAcceleration( 0 );
		//}
		//else
		//{
		//	mainActor -> GetPhysics() -> SetXAcceleration( ( float )DECELERATION );
		//}

		if( mainActor -> GetPhysics() -> GetVelocity().x < 0  && 
			stateMachine.GetCurrentState() != "jumping" && 
			stateMachine.GetCurrentState() != "doubleJump" )
		{
			mainActor -> GetPhysics() -> SetXVelocity( 0 );
			mainActor -> GetPhysics() -> SetXAcceleration( 0 );
		}
	}

	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_w ] == KEY_PRESSED &&
		stateMachine.GetCurrentState() != "jumping" &&
		stateMachine.GetCurrentState() != "doubleJump" )
	{
		Jump( jumpSpeed );
	}

	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_w ] == KEY_JUST_PRESSED &&
		stateMachine.GetCurrentState() == "jumping" )
	{
		Jump( jumpSpeed / 1.5f );
	}

	Physics* physicsBeforeCollision = new Physics( mainActor -> GetPhysics() );

	mainActor -> GetPhysics() -> Move();

	DynamicArray< Actor* >* actorList = SceneManager::GetInstance() -> GetCollidableList( mainActor -> GetID(), "mainLayer" );
	for( int i = 0; i < (*actorList).Size(); i++ )
	{
		if ( (*actorList)[ i ] -> GetPhysics() -> IsCollidable()  &&
			(*actorList)[ i ] -> GetID() != mainActor -> GetID() &&
			mainActor -> GetPhysics() -> IsCollideWith( (*actorList)[ i ] -> GetPhysics() ) )
		{
			//Event* eventc = new Event( mainActor, (*actorList)[ i ] );
			//EventBus::GetInstance() -> NotifySubscribers( (*eventc) );
			//std::cout << "Collision detected!" << std::endl;
			OnCollision( physicsBeforeCollision, (*actorList)[ i ] );
		}
	}

	delete physicsBeforeCollision;

	if( mainActor -> GetPhysics() -> GetVelocity().x > 0 )
	{
		mainActor -> SetOrientation( true );
	}
	else if( mainActor -> GetPhysics() -> GetVelocity().x < 0 )
	{
		mainActor -> SetOrientation( false );
	}

	// Change State
	stateMachine.Tick();

	// Change texture
	if( stateMachine.StateChanged() || mainActor -> OrientationChanged() )
	{
		ChangeTexture();
	}

	// Set drawable
	SDL_Rect* rect = mainActor -> GetDrawable() -> GetRect();

	// Reset drawable
	rect -> x = ( int )mainActor -> GetPhysics() -> GetPosition().x;
	rect -> y = ( int )mainActor -> GetPhysics() -> GetPosition().y;

}


void MainActorController::HandleUserEvent( SDL_Event* incomingEvent )
{
}

PlayerStateMachine* MainActorController::GetStateMachine()
{
	return &stateMachine;
}

void MainActorController::ChangeTexture()
{
	// Change Texture
	if( stateMachine.GetCurrentState() == "accelerating" || stateMachine.GetCurrentState() == "moving" )
	{
		if ( mainActor -> GetOrientation() )
		{
			mainActor-> GetDrawable() -> SetClipRectangle(  "running_r" ); 
		}
		else
		{
			mainActor-> GetDrawable() -> SetClipRectangle(  "running_l" ); 
		}
	}

	if( stateMachine.GetCurrentState() == "stopped" )
	{
		if ( mainActor -> GetOrientation() )
		{
			mainActor-> GetDrawable() -> SetClipRectangle(  "standing_r" ); 
		}
		else
		{
			mainActor-> GetDrawable() -> SetClipRectangle(  "standing_l" ); 
		}
	}

	if( stateMachine.GetCurrentState() == "jumping" || stateMachine.GetCurrentState() == "doubleJump" )
	{
		if ( mainActor -> GetOrientation() )
		{
			mainActor-> GetDrawable() -> SetClipRectangle(  "jumping_r" ); 
		}
		else
		{
			mainActor-> GetDrawable() -> SetClipRectangle(  "jumping_l" ); 
		}
	}
}

void MainActorController::Update( Event event )
{
}

void MainActorController::OnCollision( Physics* physicsBeforeCollision, Actor* collisionTaker )
{
	if( collisionTaker -> GetType() == "wall" )
	{
		float offsetX = 0;
		float offsetY = 0;
		// collide with wall
		if( mainActor -> GetPhysics() -> GetVelocity().x > 0.0f &&
			mainActor -> GetPhysics() -> GetXBoundary() > collisionTaker -> GetPhysics() -> GetPosition().x &&
			mainActor -> GetPhysics() -> GetXBoundary() < collisionTaker -> GetPhysics() -> GetXBoundary() &&
			mainActor -> GetPhysics() -> GetPosition().x < collisionTaker -> GetPhysics() -> GetPosition().x )
		{
			// offset > 0
			offsetX = mainActor -> GetPhysics() -> GetXBoundary() - collisionTaker -> GetPhysics() -> GetPosition().x;
		}

		if( mainActor -> GetPhysics() -> GetVelocity().x < 0.0f && 
			mainActor -> GetPhysics() -> GetPosition().x < collisionTaker -> GetPhysics() -> GetXBoundary() &&
			mainActor -> GetPhysics() -> GetPosition().x > collisionTaker -> GetPhysics() -> GetPosition().x &&
			mainActor -> GetPhysics() -> GetXBoundary() > collisionTaker -> GetPhysics() -> GetXBoundary() )
		{
			// offset < 0
			offsetX = mainActor -> GetPhysics() -> GetPosition().x - collisionTaker -> GetPhysics() -> GetXBoundary();
		}

		if( mainActor -> GetPhysics() -> GetVelocity().y > 0.0f &&
			mainActor -> GetPhysics() -> GetPosition().y > collisionTaker -> GetPhysics() -> GetPosition().y &&
			mainActor -> GetPhysics() -> GetPosition().y < collisionTaker -> GetPhysics() -> GetYBoundary() &&
			mainActor -> GetPhysics() -> GetYBoundary() > collisionTaker -> GetPhysics() -> GetYBoundary() )
		{
			// offset < 0
			offsetY = mainActor -> GetPhysics() -> GetPosition().y - collisionTaker -> GetPhysics() -> GetYBoundary();
		}

		if( mainActor -> GetPhysics() -> GetVelocity().y < 0.0f &&
			mainActor -> GetPhysics() -> GetYBoundary() > collisionTaker -> GetPhysics() -> GetPosition().y &&
			mainActor -> GetPhysics() -> GetYBoundary() < collisionTaker -> GetPhysics() -> GetYBoundary() &&
			mainActor -> GetPhysics() -> GetPosition().y < collisionTaker -> GetPhysics() -> GetPosition().y )
		{
			// offset > 0
			offsetY = mainActor -> GetPhysics() -> GetYBoundary() - collisionTaker -> GetPhysics() -> GetPosition().y;
		}

		if( ( abs( offsetX ) > abs( offsetY ) || ( offsetX == 0.0f ) ) && offsetY != 0.0f )
		{
			mainActor -> GetPhysics() -> GetPosition().y -= offsetY;
			mainActor -> GetPhysics() -> SetYBoundary( mainActor -> GetPhysics() -> GetYBoundary() - offsetY );
			if( offsetY > 0 )
			{
				mainActor -> GetPhysics() -> BlockYNegative();
			}
			else
			{
				mainActor -> GetPhysics() -> BlockYPositive();
			}
		}

		if( ( abs( offsetY ) > abs( offsetX ) || offsetY == 0.0f ) && offsetX != 0.0f )
		{
			mainActor -> GetPhysics() -> GetPosition().x -= offsetX;
			mainActor -> GetPhysics() -> SetXBoundary( mainActor -> GetPhysics() -> GetXBoundary() - offsetX );
			if( offsetX > 0 )
			{
				mainActor -> GetPhysics() -> BlockXPositive();
			}
			else
			{
				mainActor -> GetPhysics() -> BlockXNegative();
			}
		}
	}
}