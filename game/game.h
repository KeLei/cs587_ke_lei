#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "includes.h"
#include "../engine/SimpleEngine.h"
#include "../engine/input/KeyboardHandler.h"
#include "../engine/resource/ResourceManager.h"
#include "../engine/timer/Timer.h"
#include "player/MainActor.h"
#include "player/MainActorController.h"
#include "Factory/MinionFactory.h"
#include "Factory/BrickFactory.h"
#include "Factory/MapFactory.h"
#include "BananaController.h"

#define MAX_FPS 65

class Game
{

private:

    bool isRunning;								// Determin whether the game is running
    bool isPaused;								// Determin whether the game is paused
    SDL_Window* window;							// Pointer to the window
    SDL_Renderer* renderer;						// Pointer to renderer

	MainActor* minionActor;						// Minion Actor
	MainActorController* minionActorController;	// Controller of the minion

	Actor* banana;
	BananaController* bananaController;			// Controller of the minion

	// Test text
	Text* testText;

	Timer frameTimer;

	UserInputHandler userEvent;					// Event bus of the game

	int screenSizeH;							// Initial screen height
	int screenSizeW;							// Initial screen width


public:

    Game();										// Constructor, initialize everything with default values

    void Execute();								// Main execution		
    bool Init();								// Initialize window and renderer
	bool LoadContent();							// Load image and other stuff
    void Render();								// Render all the stuff
    bool EventLoop();							// Handle Event
    void Cleanup();								// Clean up everything
};

#endif
