#include "BananaController.h"


BananaController::BananaController()
{
}

BananaController::BananaController( Actor* banana )
{
	actor = banana;
}

BananaController::~BananaController()
{
}

void BananaController::Tick()
{
	DynamicArray< Actor* >* actorList = SceneManager::GetInstance() -> GetCollidableList( actor -> GetID(), "mainLayer" );
	for( int i = 0; i < (*actorList).Size(); i++ )
	{
		if ( (*actorList)[ i ] -> GetPhysics() -> IsCollidable()  &&
			(*actorList)[ i ] -> GetID() != actor -> GetID() &&
			actor -> GetPhysics() -> IsCollideWith( (*actorList)[ i ] -> GetPhysics() ) )
		{
			//Event* eventc = new Event( mainActor, (*actorList)[ i ] );
			//EventBus::GetInstance() -> NotifySubscribers( (*eventc) );
			//std::cout << "Collision detected!" << std::endl;
			OnCollision( NULL, (*actorList)[ i ] );
		}		
	}

	if( timer.IsStarted() )
	{
		if( timer.IsTimeout() )
		{
			OnTimeout();
			timer.StopTimer();
		}
	}

	SDL_Rect* rect = actor -> GetDrawable() -> GetRect();

	// Run physics logic
	rect -> x = ( int )actor -> GetPhysics() -> GetPosition().x;
	rect -> y = ( int )actor -> GetPhysics() -> GetPosition().y;

}

void BananaController::Update( Event event )
{
}

void BananaController::OnCollision( Physics* physicsBeforeCollision, Actor* collisionTaker )
{
	if( collisionTaker -> GetType() == "player" )
	{
		// If collide with player, move to a random position after 0.1 seconds
		timer.SetInterval( 0 );
		timer.StartTimer();
	}
	if( collisionTaker -> GetType() == "wall" )
	{
		// If collide with a wall, move to a random position immediately
		float x = Random::GetInstance() -> GetRandom( 1400 );
		float y = Random::GetInstance() -> GetRandom( 100, 750 );
		MoveTo( Vector2D( x, y ) );
	}
}

void BananaController::OnTimeout()
{
	float x = Random::GetInstance() -> GetRandom( 1400 );
	float y = Random::GetInstance() -> GetRandom( 100, 750 );
	MoveTo( Vector2D( x, y ) );
}