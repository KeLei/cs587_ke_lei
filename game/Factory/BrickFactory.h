#pragma once

#include "../../engine/scene/ActorFactory.h"

class BrickFactory : public ActorFactory
{
public:

	BrickFactory();
	~BrickFactory();

	static Actor* GetBrick( string brickName, Vector2D position, SDL_Renderer* renderer );	
};