#include "MinionFactory.h"


MinionFactory::MinionFactory()
{
}


MinionFactory::~MinionFactory()
{
}

MainActor* MinionFactory::GetMinion( SDL_Renderer* renderer )
{
	Actor* newActor = GetActor( "minion", renderer );
	MainActor* minion = new MainActor( newActor );
	delete newActor;

	minion -> GetPhysics() -> SetGravity( Vector2D( ( ( *jsonObj )[ "gravity" ].GetValueArray() )[ 0 ] -> GetNumberValue(),
		( ( *jsonObj )[ "gravity" ].GetValueArray() )[ 1 ] -> GetNumberValue() ) );
	minion -> GetPhysics() -> SetMaxSpeed( ( *jsonObj )[ "maxSpeed" ].GetNumberValue() );

	minion -> GetPhysics() -> SetVelocity( 	Vector2D( ( ( *jsonObj )[ "initialSpeed" ].GetValueArray() )[ 0 ] -> GetNumberValue(),
		( ( *jsonObj )[ "initialSpeed" ].GetValueArray() )[ 1 ] -> GetNumberValue() ) );

	return minion;
}
