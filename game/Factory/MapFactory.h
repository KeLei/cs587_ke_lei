#pragma once

#include "BrickFactory.h"
#include "../../engine/scene/SceneManager.h"
#include "../../engine/json/JsonParser.h"


class MapFactory
{

public:

	MapFactory();
	~MapFactory();

	static void GenerateMap( char* mapFilePath, std::string layerName, SDL_Renderer* renderer );
};

