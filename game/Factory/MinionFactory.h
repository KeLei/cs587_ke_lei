#pragma once

#include "../player/MainActor.h"
#include "../../engine/scene/ActorFactory.h"

class MinionFactory : public ActorFactory
{

public:

	MinionFactory();
	~MinionFactory();

	static MainActor* GetMinion( SDL_Renderer* renderer );	
};

