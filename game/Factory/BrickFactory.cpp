#include "BrickFactory.h"

BrickFactory::BrickFactory()
{}

BrickFactory::~BrickFactory()
{}

Actor* BrickFactory::GetBrick( string brickName, Vector2D position, SDL_Renderer* renderer )
{
	Actor* newActor = GetActor( brickName, renderer );

	newActor -> SetPosition( position );
	newActor -> GetPhysics() -> SetAABB(  ( float )newActor -> GetDrawable() -> GetRect() -> x,
		( float )newActor -> GetDrawable() -> GetRect() -> y,
		( float )newActor -> GetDrawable() -> GetRect() -> w,
		( float )newActor -> GetDrawable() -> GetRect() -> h );

	return newActor;
}