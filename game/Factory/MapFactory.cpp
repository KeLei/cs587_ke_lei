#include "MapFactory.h"


MapFactory::MapFactory()
{
}


MapFactory::~MapFactory()
{
}

void MapFactory::GenerateMap( char* mapFilePath, std::string layerName, SDL_Renderer* renderer )
{
	JsonValue* jsonObj = NULL;
	JsonParser parser( mapFilePath );
	if( jsonObj != NULL )
	{
		delete jsonObj;
	}
	jsonObj = parser.Parse();

	int gridSizeX = ( int )( ( *jsonObj )["grid_size"].GetValueArray()[ 0 ] ) -> GetNumberValue();
	int gridSizeY = ( int )( ( *jsonObj )["grid_size"].GetValueArray()[ 1 ] ) -> GetNumberValue();

	int numXGrid = ( int )( ( *jsonObj )["map_size"].GetValueArray()[ 0 ] ) -> GetNumberValue() / gridSizeX;
	int numYGrid = ( int )( ( *jsonObj )["map_size"].GetValueArray()[ 1 ] ) -> GetNumberValue() / gridSizeY;

	Vector2D offset( ( ( *jsonObj )["left_top_point" ].GetValueArray()[ 0 ] ) -> GetNumberValue(), 
		( ( *jsonObj )["left_top_point" ].GetValueArray()[ 1 ] ) -> GetNumberValue() );

	for( int i = 0; i < numYGrid; i++ )
	{
		for( int j = 0; j < numXGrid; j++ )
		{
			if( ( ( *jsonObj )["map_data"].GetValueArray()[ i * numXGrid + j ] ) -> GetNumberValue() == 1 )
			{
				SceneManager::GetInstance() -> AddToScene( 
					BrickFactory::GetBrick( "brick1",
						Vector2D( j * gridSizeX + offset.x, i * gridSizeY + offset.y ), renderer ), layerName );
			}
			if( ( ( *jsonObj )["map_data"].GetValueArray()[ i * numXGrid + j ] ) -> GetNumberValue() == 2 )
			{
				SceneManager::GetInstance() -> AddToScene( 
					BrickFactory::GetBrick( "brick2",
						Vector2D( j * gridSizeX + offset.x, i * gridSizeY + offset.y ), renderer ), layerName );
			}
		}
	}
}