#include "MovingState.h"


MovingState::MovingState()
{
	currentState = "moving";
}

MovingState::MovingState( Actor* actor )
{
	currentState = "moving";
	this -> actor = actor;
}

MovingState::~MovingState()
{
}

void MovingState::Tick()
{
	Physics* playerPhysics = actor -> GetPhysics();
	Vector2D currentVelocity = playerPhysics -> GetVelocity();

	if( currentVelocity.x == 0 && !playerPhysics -> MoveableYNegative() )
	{
		// Stopped
		nextState = "stopped";
	}
	else if( KeyboardHandler::GetInstance()->keyStatus[ SDLK_w ] == KEY_PRESSED ||
		playerPhysics -> MoveableYNegative() )
	{
		// Fall down while moving
		nextState = "jumping";
	}
	else
	{
		// State not changed
		nextState = "";
	}
}

std::string MovingState::GetNextState()
{
	return nextState;
}

std::string MovingState::GetCurrentState()
{
	return currentState;
}
