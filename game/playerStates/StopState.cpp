#include "StopState.h"


StopState::StopState()
{
	currentState = "stopped";
}

StopState::StopState( Actor* actor )
{
	currentState = "moving";
	this -> actor = actor;
}

StopState::~StopState()
{
}

void StopState::Tick()
{
	Physics* playerPhysics = actor -> GetPhysics();

	if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_d ] == KEY_PRESSED 
		|| KeyboardHandler::GetInstance() -> keyStatus[ SDLK_a ] == KEY_PRESSED )
	{
		nextState = "accelerating";
	}
	else if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_w ] == KEY_PRESSED )
	{
		nextState = "jumping";
	}
	else
	{
		nextState = "";
	}
}

std::string StopState::GetNextState()
{
	return nextState;
}

std::string StopState::GetCurrentState()
{
	return currentState;
}