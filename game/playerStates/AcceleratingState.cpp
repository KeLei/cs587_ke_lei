#include "AcceleratingState.h"


AcceleratingState::AcceleratingState()
{
	currentState = "accelerating";
}

AcceleratingState::AcceleratingState( Actor* actor )
{
	currentState = "accelerating";
	this -> actor = actor;
}

AcceleratingState::~AcceleratingState()
{
}

void AcceleratingState::Tick()
{
	Physics* playerPhysics = actor -> GetPhysics();
	Vector2D currentVelocity = playerPhysics -> GetVelocity();

	if( currentVelocity.x == 0 && !playerPhysics -> MoveableYNegative() )
	{
		// Stopped
		nextState = "stopped";
	}
	else if( KeyboardHandler::GetInstance()->keyStatus[ SDLK_w ] == KEY_PRESSED ||
		playerPhysics -> MoveableYNegative() )
	{
		// Jump while moving
		nextState = "jumping";
	}
	else if( currentVelocity.x >= playerPhysics -> GetMaxSpeed() && !playerPhysics -> MoveableYNegative() )
	{
		// Stopped
		nextState = "moving";
	}
	else
	{
		// State not changed
		nextState = "";
	}

}	

std::string AcceleratingState::GetNextState()
{
	return nextState;
}

std::string AcceleratingState::GetCurrentState()
{
	return currentState;
}
