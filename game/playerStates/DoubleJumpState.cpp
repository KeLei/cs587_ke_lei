#include "DoubleJumpState.h"


DoubleJumpState::DoubleJumpState()
{
	currentState = "doubleJump";
}

DoubleJumpState::DoubleJumpState( Actor* actor )
{
	currentState = "doubleJump";
	this -> actor = actor;
}

DoubleJumpState::~DoubleJumpState()
{
}

void DoubleJumpState::Tick()
{
	Physics* playerPhysics = actor -> GetPhysics();
	Vector2D currentVelocity = playerPhysics -> GetVelocity();

	if( currentVelocity.x == 0 && !playerPhysics -> MoveableYNegative() )
	{
		// Fall straight down
		nextState = "stopped";
	}
	else if( !playerPhysics -> MoveableYNegative() )
	{
		// Fall down while moving
		nextState = "moving";
	}
	else
	{
		// State not changed
		nextState = "";
	}
}

std::string DoubleJumpState::GetNextState()
{
	return nextState;
}

std::string DoubleJumpState::GetCurrentState()
{
	return currentState;
}