#pragma once

#include <string>
#include "../../engine/interface/IState.h"
#include "../../engine/scene/Actor.h"
#include "../../engine/input/KeyboardHandler.h"

class AcceleratingState : public IState
{
private:

	// Actor that hold this state
	Actor* actor;

	// String indicating next state
	std::string nextState;

	// String indicating current state
	std::string currentState;

public:

	AcceleratingState();
	AcceleratingState( Actor* actor );
	~AcceleratingState();

	void Tick();
	std::string GetNextState();
	std::string GetCurrentState();
};

