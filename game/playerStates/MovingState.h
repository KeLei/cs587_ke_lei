#pragma once

#include <string>
#include "../../engine/interface/IState.h"
#include "../../engine/scene/Actor.h"
#include "../../engine/input/KeyboardHandler.h"

class MovingState : public IState
{

private:

	// Actor that hold this state
	Actor* actor;

	// String indicating next state
	std::string nextState;

	// String indicating current state
	std::string currentState;

public:

	MovingState();
	MovingState( Actor* actor );
	~MovingState();

	void Tick();
	std::string GetNextState();
	std::string GetCurrentState();
};

