#pragma once

#include <string>
#include "../../engine/interface/IState.h"
#include "../../engine/scene/Actor.h"
#include "../../engine/input/KeyboardHandler.h"

/*
* State that player is jumping
*/

class DoubleJumpState : public IState
{

private:

	// Actor that hold this state
	Actor* actor;

	// String indicating next state
	std::string nextState;

	// String indicating current state
	std::string currentState;

public:

	DoubleJumpState();
	DoubleJumpState( Actor* actor );
	~DoubleJumpState();

	void Tick();
	std::string GetNextState();
	std::string GetCurrentState();
};

