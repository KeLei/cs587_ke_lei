#include "JumpState.h"


JumpState::JumpState()
{
	currentState = "jump";
}

JumpState::JumpState( Actor* actor )
{
	currentState = "jump";
	this -> actor = actor;
}

JumpState::~JumpState()
{
}

void JumpState::Tick()
{
	Physics* playerPhysics = actor -> GetPhysics();
	Vector2D currentVelocity = playerPhysics -> GetVelocity();

	if( currentVelocity.x == 0 && !playerPhysics -> MoveableYNegative() )
	{
		// Fall straight down
		nextState = "stopped";
	}
	else if( !playerPhysics -> MoveableYNegative() )
	{
		// Fall down while moving
		nextState = "moving";
	}
	else if( KeyboardHandler::GetInstance() -> keyStatus[ SDLK_w ] == KEY_JUST_PRESSED )
	{
		// State not changed
		nextState = "doubleJump";
	}
	else
	{
		// State not changed
		nextState = "";
	}
}

std::string JumpState::GetNextState()
{
	return nextState;
}

std::string JumpState::GetCurrentState()
{
	return currentState;
}