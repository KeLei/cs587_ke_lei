#pragma once

#include "../engine/scene/ActorController.h"
#include "../engine/scene/SceneManager.h"
#include "../engine/misc/Random.h"
#include "../engine/interface/ITimeout.h"
#include "../engine/Timer/Timer.h"

class BananaController : public ActorController, ITimeout
{
private:

	Timer timer;

public:
	BananaController();
	BananaController( Actor* banana );
	~BananaController();

	void Tick();
	void Update( Event incomingEvent );
	void OnCollision( Physics* physicsBeforeCollision, Actor* collisionTaker );

	void OnTimeout();
};

