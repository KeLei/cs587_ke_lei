# CS 587 - Game Engine Design

[Assets Link](https://drive.google.com/folderview?id=0B5_qO73NHyspdlNyeER5MWxmX0k&usp=sharing)

## Style Guide:

* All operators should appear with one space on both side.
  *operators include: + - * / ^ & && = < > == <= >= += -= -> | 

* Between parentheses, if no arguements than no space, else leave one space at both side of the parentheses.
  e.g.: Init();
        list.addToList( T object );

* All class and functions capitalize first alphabet, variables begin with lowercase, marocs capitalize ALL.
  e.g.: MyActorController* actorController;
        actor -> MoveLeft();
        #define AI_CONTROLLER 1

* Braces should always use a new line which contains only the braces.
  e.g.:if( Init() == false )
       {
          return;
       }

## Objections

* Scene management
* Event bus
* State machine
* Resource management
* Input management
* Quad tree collision detection
* Json parser
* Game created upon the engine

## Library dependencies - SDL

[SDL-Simple DirectMedia Layer](http://www.libsdl.org/)

Used for window and renderer, image and sound loading

## Structure

### engine

#### event

Contains the event bus

#### input

Contains input management

#### interface

Contains all interfaces used in engine

#### json

Contains json parser

#### misc

Classes that don't fit into any other folders

#### quadtree

Contains quad tree

#### resource

Contains resource manager

##### scene

Contains actor, actor controller...

#### structure

Data structures

### game

#### Factory

Different factories generate all kind of actors

#### player

All stuff about the main player

#### playerStates

States of the player

## Contact

Ke Lei: leikerex91@gmail.com